<?php

 /**
 * @category  TM
 * @package   TM_Quickrfq
 * @author    Anjan Barik<AB00350686@techmahindra.com>
 * @copyright Anjan Barik<AB00350686@techmahindra.com>
 * 
 */
 
class FME_Quickrfq_Block_Adminhtml_Quickrfq_Edit_Tab_Rfqproduct extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
        parent::__construct();
        $this->setId('productGrid');        
        $this->setDefaultSort('entity_id');        
        $this->setDefaultDir('ASC');
        $this->setDefaultLimit(20);
        $this->setSaveParametersInSession(false); 
    }

    protected function _prepareCollection() {
        $rfqId = (int) $this->getRequest()->getParam('id');
        $collection = Mage::getResourceModel('quickrfq/rfqproduct_collection')
                ->addFieldToFilter('quickrfq_id',$rfqId);        
        $this->setCollection($collection);
        parent::_prepareCollection();       
        return $this;
    }


    protected function _prepareColumns() {

        $this->addColumn('entity_id', array(
            'header' => Mage::helper('quickrfq')->__('Product Id'),
            'sortable' => true,
            'width' => '60px',
            'index' => 'entity_id'
        ));

        $this->addColumn('product_name', array(
            'header' => Mage::helper('quickrfq')->__('Name'),
            'index' => 'product_name'
        ));
        
        $this->addColumn('product_price', array(
            'header' => Mage::helper('quickrfq')->__('Price'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'product_price'
        ));
        
        $this->addColumn('product_qty', array(
            'header' => Mage::helper('quickrfq')->__('Product Quanity'),
            'width' => '120px',
            'index' => 'product_qty'
        ));
        return parent::_prepareColumns();
    }
}