<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 */

/** @var $installer Magestore_RewardPointsRule_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();
$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('rfqproduct')};
CREATE TABLE {$this->getTable('rfqproduct')} (
  `rfqproduct_id` int(11) unsigned NOT NULL auto_increment,
  `quickrfq_id` int(11) unsigned NOT NULL,
  `entity_id` int(11) unsigned NOT NULL,
  `product_name` varchar(255) NOT NULL default '',
  `product_price` varchar(25) NOT NULL default '',
  `product_qty` int(11) NOT NULL default '1',      
  PRIMARY KEY (`rfqproduct_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE {$this->getTable('rfqproduct')}
    ADD CONSTRAINT `FK_QUICKRFQ` FOREIGN KEY (`quickrfq_id`)
    REFERENCES {$this->getTable('quickrfq')} (`quickrfq_id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE;
");

$installer->endSetup();
