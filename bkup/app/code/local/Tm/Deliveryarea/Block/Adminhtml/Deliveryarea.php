<?php
class Tm_Deliveryarea_Block_Adminhtml_Deliveryarea extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_deliveryarea';
    $this->_blockGroup = 'deliveryarea';
    $this->_headerText = Mage::helper('deliveryarea')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('deliveryarea')->__('Add Item');
    parent::__construct();
  }
}