<?php

class Tm_Deliveryarea_Block_Adminhtml_Deliveryarea_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('deliveryarea_form', array('legend' => Mage::helper('deliveryarea')->__('Pin Code Information')));

        $fieldset->addField('city_name', 'text', array(
            'label' => Mage::helper('deliveryarea')->__('City Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'city_name',
        ));
        $fieldset->addField('state_name', 'text', array(
            'label' => Mage::helper('deliveryarea')->__('State Name'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'state_name',
        ));
        $fieldset->addField('city_pincode', 'text', array(
            'label' => Mage::helper('deliveryarea')->__('Pin Code'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'city_pincode',
        ));


        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('deliveryarea')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('deliveryarea')->__('Enabled'),
                ),
                array(
                    'value' => 2,
                    'label' => Mage::helper('deliveryarea')->__('Disabled'),
                ),
            ),
        ));

        if (Mage::getSingleton('adminhtml/session')->getDeliveryareaData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getDeliveryareaData());
            Mage::getSingleton('adminhtml/session')->setDeliveryareaData(null);
        } elseif (Mage::registry('deliveryarea_data')) {
            $form->setValues(Mage::registry('deliveryarea_data')->getData());
        }
        return parent::_prepareForm();
    }

}