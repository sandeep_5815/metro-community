<?php

class Tm_Deliveryarea_Block_Adminhtml_Deliveryarea_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'deliveryarea';
        $this->_controller = 'adminhtml_deliveryarea';
        
        $this->_updateButton('save', 'label', Mage::helper('deliveryarea')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('deliveryarea')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('deliveryarea_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'deliveryarea_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'deliveryarea_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('deliveryarea_data') && Mage::registry('deliveryarea_data')->getId() ) {
            return Mage::helper('deliveryarea')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('deliveryarea_data')->getTitle()));
        } else {
            return Mage::helper('deliveryarea')->__('Add Item');
        }
    }
}