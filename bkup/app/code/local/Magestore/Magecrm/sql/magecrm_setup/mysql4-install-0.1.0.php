<?php

$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('magecrm')};
CREATE TABLE {$this->getTable('magecrm')} (
  `magecrm_id` int(11) unsigned NOT NULL auto_increment,
  `data_transfer` varchar(255) NOT NULL default '',
  `id_from` int(11)  ,
  `total` int(11)  ,
  `time_transfer` datetime NULL,
  `status` smallint(6) NOT NULL default '0',
  `message` varchar(255) NOT NULL default '',
  PRIMARY KEY (`magecrm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 


