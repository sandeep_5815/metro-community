<?php

$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('magecrm_mapping_product')};
CREATE TABLE {$this->getTable('magecrm_mapping_product')} (
  `map_product_id` int(11) unsigned NOT NULL auto_increment,
  `magento_name` varchar(255) ,
  `magento_label` varchar(255) ,
  `crm`   varchar(255),
  `crm_module` varchar(255) ,
  `crm_name` varchar(255)  ,
  `crm_label` varchar(255),
  `type`    varchar(100)NOT NULL default 'field',
  `code` varchar(255) ,
  PRIMARY KEY (`map_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('mapping')};
CREATE TABLE {$this->getTable('mapping')} (
  `map_id` int(11) unsigned NOT NULL auto_increment,
  `data_type` varchar(255) ,
  `magento_name` varchar(255) ,
  `magento_label` varchar(255) ,
  `crm`   varchar(255),
  `crm_module` varchar(255) ,
  `crm_name` varchar(255)  ,
  `crm_label` varchar(255),
  `type`    varchar(100)NOT NULL default 'field',
  `code` varchar(255) ,
  PRIMARY KEY (`map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");



$installer->endSetup();
