<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   design_default
 * @package    Mage
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
?>
<?php $_order = $this->getOrder() ?>
<?php
$orderAdminDate = $this->formatDate($_order->getCreatedAtDate(), 'medium', true);
$orderStoreDate = $this->formatDate($_order->getCreatedAtStoreDate(), 'medium', true);
?>
<div class="box-left">
    <!--Order Information-->
    <div class="entry-edit">
        <?php if ($_order->getEmailSent()):
            $_email=$this->__('Order confirmation email sent');
        else:
            $_email=$this->__('Order confirmation email not sent');
        endif; ?>
        <div class="entry-edit-head">
        <?php if ($this->getNoUseOrderLink()): ?>
            <h4 class="icon-head head-account"><?php echo Mage::helper('sales')->__('Order # %s', $_order->getRealOrderId()) ?> (<?php echo $_email ?>)</h4>
        <?php else: ?>
            <a href="<?php echo $this->getViewUrl($_order->getId()) ?>"><?php echo Mage::helper('sales')->__('Order # %s', $_order->getRealOrderId()) ?></a>
            <strong>(<?php echo $_email ?>)</strong>
        <?php endif; ?>
        </div>
        <div class="fieldset">
            <table cellspacing="0" class="form-list">
            <tr>
                <td class="label"><label><?php echo Mage::helper('sales')->__('Order Date') ?></label></td>
                <td><strong><?php echo $orderAdminDate ?></strong></td>
            </tr>
            <?php if ($orderAdminDate != $orderStoreDate):?>
            <tr>
                <td class="label"><label><?php echo Mage::helper('sales')->__('Order Date (%s)', $_order->getCreatedAtStoreDate()->getTimezone()) ?></label></td>
                <td><strong><?php echo $orderStoreDate ?></strong></td>
            </tr>
            <?php endif;?>
            <tr>
                <td class="label"><label><?php echo Mage::helper('sales')->__('Order Status') ?></label></td>
                <td><strong><span id="order_status"><?php echo $_order->getStatusLabel() ?></span></strong></td>
            </tr>
            <tr>
                <td class="label"><label><?php echo Mage::helper('sales')->__('Purchased From') ?></label></td>
                <td><strong><?php echo $this->getOrderStoreName() ?></strong></td>
            </tr>
            <?php if($_order->getRelationChildId()): ?>
            <tr>
                <td class="label"><label><?php echo Mage::helper('sales')->__('Link to the new order') ?></label></td>
                <td><a href="<?php echo $this->getViewUrl($_order->getRelationChildId()) ?>">
                    <?php echo $_order->getRelationChildRealId() ?>
                </a></td>
            </tr>
            <?php endif; ?>
            <?php if($_order->getRelationParentId()): ?>
            <tr>
                <td class="label"><label><?php echo Mage::helper('sales')->__('Link to the previous order') ?></label></td>
                <td><a href="<?php echo $this->getViewUrl($_order->getRelationParentId()) ?>">
                    <?php echo $_order->getRelationParentRealId() ?>
                </a></td>
            </tr>
            <?php endif; ?>
            <?php if($_order->getRemoteIp()): ?>
            <tr>
                <td class="label"><label><?php echo Mage::helper('sales')->__('Placed from IP') ?></label></td>
                <td><strong><?php echo $_order->getRemoteIp() ?></strong></td>
            </tr>
            <?php endif; ?>
            <?php if($_order->getGlobalCurrencyCode() != $_order->getBaseCurrencyCode()): ?>
            <tr>
                <td class="label"><label><?php echo Mage::helper('sales')->__('%s / %s rate:', $_order->getGlobalCurrencyCode(), $_order->getBaseCurrencyCode()) ?></label></td>
                <td><strong><?php echo $_order->getBaseToGlobalRate() ?></strong></td>
            </tr>
            <?php endif; ?>
            <?php if($_order->getBaseCurrencyCode() != $_order->getOrderCurrencyCode()): ?>
            <tr>
                <td class="label"><label><?php echo Mage::helper('sales')->__('%s / %s rate:', $_order->getOrderCurrencyCode(), $_order->getBaseCurrencyCode()) ?></label></td>
                <td><strong><?php echo $_order->getBaseToOrderRate() ?></strong></td>
            </tr>
            <?php endif; ?>
            </table>
        </div>
    </div>
</div>
<div class="box-right">
    <!--Account Information-->
    <div class="entry-edit">
        <div class="entry-edit-head">
            <h4 class="icon-head head-account"><?php echo Mage::helper('sales')->__('Account Information') ?></h4>
        </div>
        <div class="fieldset">
            <div class="hor-scroll">
                <table cellspacing="0" class="form-list">
                <tr>
                    <td class="label"><label><?php echo Mage::helper('sales')->__('Customer Name') ?></label></td>
                    <td>
                    <?php if ($_customerUrl=$this->getCustomerViewUrl()) : ?>
                        <a href="<?php echo $_customerUrl ?>" target="_blank"><strong><?php echo $this->htmlEscape($_order->getCustomerName()) ?></strong></a>
                    <?php else: ?>
                        <strong><?php echo $this->htmlEscape($_order->getCustomerName()) ?></strong>
                    <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="label"><label><?php echo Mage::helper('sales')->__('Email') ?></label></td>
                    <td><a href="mailto:<?php echo $_order->getCustomerEmail() ?>"><strong><?php echo $_order->getCustomerEmail() ?></strong></a></td>
                </tr>
                <?php if ($_groupName=$this->getCustomerGroupName()) : ?>
                <tr>
                    <td class="label"><label><?php echo Mage::helper('sales')->__('Customer Group') ?></label></td>
                    <td><strong><?php echo $_groupName ?></strong></td>
                </tr>
                <?php endif; ?>
                <?php if ($_dob=$this->getOrder()->getCustomerDob()) : ?>
                <tr>
                    <td class="label"><label><?php echo Mage::helper('sales')->__('Date of Birth') ?></label></td>
                    <td><strong><?php echo Mage::helper('core')->formatDate($_dob, 'medium') ?></strong></td>
                </tr>
                <?php endif; ?>
                <?php if ($_taxvat = $_order->getCustomerTaxvat()):?>
                <tr>
                    <td class="label"><label><?php echo Mage::helper('sales')->__('TAX/VAT Number') ?></label></label></td>
                    <td><strong><?php echo $this->htmlEscape($_taxvat)?></strong></td>
                </tr>
                <?php endif;?>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>

<div class="box-left">
    <!--Billing Address-->
    <div class="entry-edit">
        <div class="entry-edit-head">
            <h4 class="icon-head head-billing-address"><?php echo Mage::helper('sales')->__('Billing Address') ?></h4>
        </div>
        <fieldset>
            <address><?php echo $_order->getBillingAddress()->getFormated(true) ?></address>
        </fieldset>
    </div>
</div>
<?php if (!$this->getOrder()->getIsVirtual()): ?>
<div class="box-right">
    <!--Shipping Address-->
    <div class="entry-edit">
        <div class="entry-edit-head">
            <h4 class="icon-head head-shipping-address"><?php echo Mage::helper('sales')->__('Shipping Address') ?></h4>
        </div>
        <fieldset>
            <address><?php echo $_order->getShippingAddress()->getFormated(true) ?></address>
        </fieldset>
    </div>
</div>
<div class="clear"></div>
<?php endif; ?>
<?php 	
	$ddate = Mage::getResourceModel('ddate/ddate')->getDdateByOrder($this->getOrder()->getIncrementId());
	$orderid=$this->getOrder()->getIncrementId();
?>
<div class="box-left">
    <!--Delivery Date-->
    <div class="entry-edit">
       
	    
		
				<div class="entry-edit-head">
			<h4 class="icon-head head-shipping-method"><?php echo $this->helper('ddate')->__('Delivery Date') ?></h4>
			<div class="tools">
				<a id="mw_delivery_edit_button" name="mw_delivery_edit_button" href="javascript:void(0);" onclick="show_delivery_form()">Edit</a>
				<a style="display:none" id="mw_delivery_done_button" name="mw_delivery_done_button" href="javascript:void(0);" onclick="update_delivery_form()">Finish</a>
			</div>
		
		</div>
		<fieldset>
		<?php if($ddate):?>
		<div id="current_mwdelivery_info">	
			
			<?php echo $this->__('Delivery Date:')?><span id="mwcurrent_date" name ="mwcurrent_date" style="font-weight:bold;"> <?php echo $ddate['ddate'];?></span><br>
			
			
			<?php echo $this->__('Delivery Time:')?><span  id="mwcurrent_time" name ="mwcurrent_time" style="font-weight:bold;"> <?php echo $ddate['dtime'];?></span></br>
			
		</div>
		<div style="display:none" id="edit_mwdelivery_info">
		
			<?php echo $this->__('Delivery Date:')?><span style="font-weight:bold;">
			
				<input type="text" name="onestepcheckout_date" id="onestepcheckout_date" value="<?php echo $ddate['ddate'];?>" class="required-entry" /> 
				<img title="Select date" id="cal_date_trig" src="<?php echo Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN) . 'adminhtml/default/default/images/grid-cal.gif'; ?>"  class="v-middle"/>
				 
				 
			 </span><br>			 		 
						
								<?php echo $this->__('Delivery Time:')?><span style="font-weight:bold;">		 			
								<?php 
										$dtimes = Mage::helper('ddate')->getDtime($_order->getStoreId());
										
									
									?>
									<div style="width:95px;float:right;width:398px;">
										<select id="onestepcheckout_time" size="1" name="onestepcheckout_time"  class="required-entry" >
													<option value=""><?php echo $this->__('Time Range');?></option>
											<?php foreach($dtimes as $slot):?>
												<?php if($slot->getStatus()):?>
													<option value="<?php echo $slot->getDtimeId()?>" <?php if($ddate['dtime']==$slot->getDtime()):?>selected<?php endif;?>><?php echo $slot->getDtime();?></option>
												<?php endif?>
											<?php endforeach?>
										</select>
									</div>
								  </span>
									
							
							</br>		
				
		
		</div>
		<?php else:?>
		<div id="current_mwdelivery_info">	
			
			<?php echo $this->__('Delivery Date:')?><span id="mwcurrent_date" name ="mwcurrent_date" style="font-weight:bold;"> </span><br>
			
			
			<?php echo $this->__('Delivery Time:')?><span  id="mwcurrent_time" name ="mwcurrent_time" style="font-weight:bold;"> </span>
			
		</div>
		
		
		<div style="display:none" id="edit_mwdelivery_info">
		
			<?php echo $this->__('Delivery Date:')?><span style="font-weight:bold;">
			
				<input type="text" name="onestepcheckout_date" id="onestepcheckout_date"  class="required-entry" /> 
				<img title="Select date" id="cal_date_trig" src="<?php echo Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN) . 'adminhtml/default/default/images/grid-cal.gif'; ?>"  class="v-middle"/>
				 
				 
			 </span><br>			 		 
						
								<?php echo $this->__('Delivery Time:')?><span style="font-weight:bold;">		 			
								<?php 
										$dtimes = Mage::helper('ddate')->getDtime($_order->getStoreId());							
									?>
									<div style="width:95px;float:right;width:398px;">
										<select id="onestepcheckout_time" size="1" name="onestepcheckout_time"  class="required-entry" >
													<option value=""><?php echo $this->__('Time Range');?></option>
											<?php foreach($dtimes as $slot):?>
												<?php if($slot->getStatus()):?>
													<option value="<?php echo $slot->getDtimeId()?>" ><?php echo $slot->getDtime()?></option>
												<?php endif?>
											<?php endforeach?>
										</select>
									</div>
								  </span>													
		
		</div>
		
		<?php endif?>			
		
		</fieldset>		
    </div>
</div>

<div class="box-right">
    <div class="entry-edit">	
						<div class="entry-edit-head">
			<h4 class="icon-head head-shipping-method"><?php echo $this->helper('ddate')->__('Customer Order Comment') ?></h4>
			<div class="tools">
				<a id="mw_comment_edit_button" name="mw_comment_edit_button" href="javascript:void(0);" onclick="show_comment_form()">Edit</a>
				<a style="display:none" id="mw_comment_done_button" name="mw_comment_done_button" href="javascript:void(0);" onclick="update_comment_form()">Finish</a>
			</div>
		</div>
		
		<fieldset>
		<?php if(!empty($ddate['ddate_comment'])):?>
				<p id="mwcomment" name="mwcomment">
				<?php echo $ddate['ddate_comment']; ?>
				</p>				
				<textarea cols="84" rows="2" value="<?php echo $ddate['ddate_comment']; ?>" style="display:none;" name="mw_customercomment_info" id="mw_customercomment_info" style="width:98%; height:8em;"> <?php echo $ddate['ddate_comment']; ?></textarea>
		<?php else:?>
			<p id="mwcomment" name="mwcomment">
				<?php echo '' ?>
				</p>	
				<textarea cols="84" rows="2" style="display:none;" name="mw_customercomment_info" id="mw_customercomment_info" style="width:98%; height:8em;"> </textarea>
		<?php endif?>
			
		
		</fieldset>
		
		
		
		
    </div>
</div>

<div class="clear"></div>

<script type="text/javascript">// <![CDATA[
    Calendar.setup({
        inputField : 'onestepcheckout_date',
        ifFormat : '%Y-%m-%d',
        button : 'cal_date_trig',
        align : 'Bl',
        singleClick : true
    });
	
	deliveryurls= '<?php echo Mage::getUrl('ddate_admin/adminhtml_ddate/update_delivery') ?>';	
		mwdeliveryorderid= '<?php echo $orderid; ?>';
		function show_delivery_form()
		{
			//alert("Hello World!");			
			$('mw_delivery_edit_button').hide();
			$('mw_delivery_done_button').show();
			
			$('current_mwdelivery_info').hide();
			$('edit_mwdelivery_info').show();
			
		}
		function update_delivery_form()
		{
			//var temple_text = "";
			var date_post ="orderid="+mwdeliveryorderid+"&mwdate="+$('onestepcheckout_date').getValue();
			if($('onestepcheckout_time')) date_post=date_post+"&mwtime="+$('onestepcheckout_time').getValue();
			if($('mw_customercomment_info')) date_post=date_post+"&mwcomment="+$('mw_customercomment_info').getValue();
			if($('mw_customer_text')){  //temple_text =$('mw_customer_text').getValue(); 
			date_post=date_post+"&mwcustomertext="+ $('mw_customer_text').getValue(); };
			//alert("Hello World!");
			$('mw_delivery_edit_button').show();
			$('mw_delivery_done_button').hide();
			 data= date_post ,
		 	new Ajax.Request( deliveryurls,
			{
			 method:'post',
			
			 parameters: data,
			 onSuccess: function(transport){
			   var response = transport.responseText || "no response text";
			   alert(response);
			   $('mwcurrent_date').update($('onestepcheckout_date').getValue());
			   if($('onestepcheckout_time')) $('mwcurrent_time').update($('onestepcheckout_time')[$('onestepcheckout_time').selectedIndex].text);
			   },
			 onFailure: function(){ alert('Something went wrong...') }
			});	 
			
			$('current_mwdelivery_info').show();
			$('edit_mwdelivery_info').hide();
		}	
	</script>
	
	 <script>
		commenturls= '<?php echo Mage::getUrl('ddate_admin/adminhtml_ddate/update_comment') ?>';
		mworderid= '<?php echo $orderid; ?>';
		function show_comment_form()
		{
			//alert("Hello World!");			
			$('mw_comment_edit_button').hide();
			$('mw_comment_done_button').show();
			$('mwcomment').hide();
			$('mw_customercomment_info').show();
			
		}
		function update_comment_form()
		{
			//alert("Hello World!");
			$('mw_comment_edit_button').show();
			$('mw_comment_done_button').hide();
			 data= "orderid="+mworderid+"&mwcomment="+$('mw_customercomment_info').getValue();
			new Ajax.Request( commenturls,
			{
			 method:'post',
			
			 parameters: data,
			 onSuccess: function(transport){
			   var response = transport.responseText || "no response text";
			   alert(response);
			   $('mwcomment').update($('mw_customercomment_info').getValue());
			   },
			 onFailure: function(){ alert('Something went wrong...') }
			});			
			$('mwcomment').show();
			$('mw_customercomment_info').hide();
		}
	</script> 
	
	

