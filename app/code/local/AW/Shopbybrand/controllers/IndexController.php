<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Shopbybrand
 * @version    1.3.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Shopbybrand_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function allBrandsViewAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function brandPageViewAction()
    {
        $brandId = $this->getRequest()->getParam('brand_id', null);
        if (null === $brandId) {
            $this->_redirect('cms/noRoute');
        }

        $brandModel = Mage::getModel('awshopbybrand/brand')->load($brandId);
        if (null === $brandModel->getId()) {
            $this->_redirect('cms/noRoute');
        }

        Mage::register('current_brand', $brandModel);

        $this->loadLayout();
        $this->getRequest()
            ->setActionName($brandModel->getUrlKey())
            ->setControllerName('')
            ->setRouteName(Mage::helper('awshopbybrand/config')->getAllBrandsUrlKey())
        ;

        $this->renderLayout();
    }
}