<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

try {
    $this->startSetup();
    $this->run("
    CREATE TABLE IF NOT EXISTS {$this->getTable('betterthankyoupage/rule')} (
      `rule_id` int(11) NOT NULL AUTO_INCREMENT,
      `title` varchar(255) NOT NULL,
      `description` text NOT NULL,
      `is_active` tinyint(4) NOT NULL,
      `store_ids` varchar(255) NOT NULL,
      `customer_group_ids` varchar(255) NOT NULL,
      `from_date` date DEFAULT NULL,
      `to_date` date DEFAULT NULL,
      `priority` int(11) NOT NULL,
      `conditions_serialized` text NOT NULL,
      `orderinfo_display` tinyint(4) DEFAULT NULL,
      `orderinfo_sort` int(11) DEFAULT NULL,
      `orderinfo_text` text DEFAULT NULL,
      `cms_display` tinyint(4) DEFAULT NULL,
      `cms_sort` int(11) DEFAULT NULL,
      `cms_block_id` int(11) DEFAULT NULL,
      PRIMARY KEY (`rule_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");
    $this->endSetup();
} catch (Exception $e) {
    echo $e->getMessage();
    Mage::logException($e);
}
