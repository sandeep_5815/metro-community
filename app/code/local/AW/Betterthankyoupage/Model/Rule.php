<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Betterthankyoupage_Model_Rule extends AW_Betterthankyoupage_Model_RuleCommon
{

    public function getConditionsInstance()
    {
        return Mage::getModel('betterthankyoupage/rule_condition_combine');
    }

    public function getActionsInstance()
    {
        return Mage::getModel('rule/action_collection');
    }

    public function validateData(Varien_Object $object)
    {
        $result   = array();
        $fromDate = $toDate = null;

        if ($object->hasFromDate() && $object->hasToDate()) {
            $fromDate = $object->getFromDate();
            $toDate = $object->getToDate();
        }

        if ($fromDate && $toDate) {
            $fromDate = new Zend_Date($fromDate, Varien_Date::DATE_INTERNAL_FORMAT);
            $toDate = new Zend_Date($toDate, Varien_Date::DATE_INTERNAL_FORMAT);

            if ($fromDate->compare($toDate) === 1) {
                $result[] = Mage::helper('betterthankyoupage')->__('End Date must be greater than Start Date.');
            }
        }

        if ($object->hasStoreIds()) {
            $storeIds = $object->getStoreIds();
            if (empty($storeIds)) {
                $result[] = Mage::helper('betterthankyoupage')->__('Sites must be specified.');
            }
        }

        if ($object->hasCustomerGroupIds()) {
            $groupIds = $object->getCustomerGroupIds();
            if (empty($groupIds)) {
                $result[] = Mage::helper('betterthankyoupage')->__('Customer Groups must be specified.');
            }
        }

        if ($object->getPriority() && !Zend_Validate::is($object->getPriority(), 'Int')) {
            $result[] = Mage::helper('betterthankyoupage')->__('Priority must be an integer. Only one rule with greater priority will be processed.');
        }

        if (
            (
                is_null($object->getUseConfigOrderinfoSort())
                && $object->getOrderinfoSort()
                && !Zend_Validate::is($object->getOrderinfoSort(), 'Int')
            )  || (
                is_null($object->getUseConfigCmsSort())
                && $object->getCmsSort()
                && !Zend_Validate::is($object->getCmsSort(), 'Int')
            )
        ) {
            $result[] = Mage::helper('betterthankyoupage')->__('Sort order must be an integer. Less number means higher position on the page.');
        }

        return !empty($result) ? $result : true;
    }

    protected function _construct()
    {
        parent::_construct();
        $this->_init('betterthankyoupage/rule');
        $this->setIdFieldName('rule_id');
    }

}
