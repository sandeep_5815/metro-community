<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Betterthankyoupage_Model_Resource_Rule extends AW_Betterthankyoupage_Model_Resource_RuleCommon
{

    protected function _construct()
    {
        $this->_init('betterthankyoupage/rule', 'rule_id');
    }

    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        parent::_afterLoad($object);
        if ($object->getData('from_date') == '0000-00-00') {
            $object->setData('from_date', null);
        }
        if ($object->getData('to_date') == '0000-00-00') {
            $object->setData('to_date', null);
        }
        $object->setDataChanges(false);
        return $this;
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        if (is_array($object->getData('store_ids'))) {
            $object->setData('store_ids', implode(',', $object->getData('store_ids')));
        }
        if (is_array($object->getData('customer_group_ids'))) {
            $object->setData('customer_group_ids', implode(',', $object->getData('customer_group_ids')));
        }
        if ($object->getData('use_config_orderinfo_display') !== null) {
            $object->setData('orderinfo_display', null);
            $object->unsetData('use_config_orderinfo_display');
        }
        if ($object->getData('use_config_orderinfo_sort') !== null) {
            $object->setData('orderinfo_sort', null);
            $object->unsetData('use_config_orderinfo_sort');
        }
        if ($object->getData('use_config_orderinfo_text') !== null) {
            $object->setData('orderinfo_text', null);
            $object->unsetData('use_config_orderinfo_text');
        }
        if ($object->getData('use_config_cms_display') !== null) {
            $object->setData('cms_display', null);
            $object->unsetData('use_config_cms_display');
        }
        if ($object->getData('use_config_cms_sort') !== null) {
            $object->setData('cms_sort', null);
            $object->unsetData('use_config_cms_sort');
        }
        if ($object->getData('use_config_cms_block_id') !== null) {
            $object->setData('cms_block_id', null);
            $object->unsetData('use_config_cms_block_id');
        }
        if ($object->getData('orderinfo_sort') !== null) {
            $object->setData('orderinfo_sort', intval($object->getData('orderinfo_sort')));
        }
        if ($object->getData('cms_sort') !== null) {
            $object->setData('cms_sort', intval($object->getData('cms_sort')));
        }
        $object->setData('priority', intval($object->getData('priority')));
        return parent::_beforeSave($object);
    }

}
