<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Betterthankyoupage_Model_Resource_Rule_Collection extends AW_Betterthankyoupage_Model_Resource_Rule_CollectionCommon
{

    protected function _construct()
    {
        $this->_init('betterthankyoupage/rule');
    }

    public function setActiveFilter()
    {
        $this->addFieldToFilter('is_active', '1');
        return $this;
    }

    public function setDateFilter(Zend_Date $date)
    {
        $this->addFieldToFilter(
            'from_date',
            array(
                array('lteq' => $date->toString(Varien_Date::DATE_INTERNAL_FORMAT)),
                array('eq' => '0000-00-00'),
                array('null' => true)
            )
        );
        $this->addFieldToFilter(
            'to_date',
            array(
                array('gteq' => $date->toString(Varien_Date::DATE_INTERNAL_FORMAT)),
                array('eq' => '0000-00-00'),
                array('null' => true)
            )
        );
        return $this;
    }

    public function setCustomerGroupFilter($customerGroupId)
    {
        if (Mage::helper('betterthankyoupage')->isMageVersionFit()) {
            $this->addFieldToFilter(
                'customer_group_ids',
                array('regexp' => '(,|^)' . $customerGroupId . '(,|$)')
            );
        } else {
            $this->addFieldToFilter(
                'customer_group_ids',
                array(
                    array('like' => '%,' . $customerGroupId . ',%'),
                    array('like' => $customerGroupId . ',%'),
                    array('like' => '%,' . $customerGroupId),
                    array('eq' => $customerGroupId)
                )
            );
        }
        return $this;
    }

    public function setStoreFilter($storeId)
    {
        if (Mage::helper('betterthankyoupage')->isMageVersionFit()) {
            $this->addFieldToFilter(
                'store_ids',
                array(
                    array('regexp' => '(,|^)' . $storeId . '(,|$)'),
                    array('eq' => '0')
                )
            );
        } else {
            $this->addFieldToFilter(
                'store_ids',
                array(
                    array('like' => '%,' . $storeId . ',%'),
                    array('like' => $storeId . ',%'),
                    array('like' => '%,' . $storeId),
                    array('eq' => $storeId),
                    array('eq' => '0')
                )
            );
        }
        return $this;
    }

    public function setPriorityOrder()
    {
        $this->setOrder('priority', Varien_Data_Collection::SORT_ORDER_DESC);
        return $this;
    }

    protected function _afterLoad()
    {
        parent::_afterLoad();
        /**
         * @var $item Varien_Object
         */
        foreach ($this->getItems() as $item) {
            if ($item->getData('from_date') == '0000-00-00') {
                $item->setData('from_date', null);
            }
            if ($item->getData('to_date') == '0000-00-00') {
                $item->setData('to_date', null);
            }
            $item->setDataChanges(false);
        }
        return $this;
    }

}
