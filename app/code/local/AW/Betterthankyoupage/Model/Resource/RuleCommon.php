<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

if (
    Mage::helper('betterthankyoupage')->isMageVersionFit()
    && class_exists('Mage_Core_Model_Resource_Db_Abstract')
) {
    abstract class AW_Betterthankyoupage_Model_Resource_RuleCommon extends Mage_Core_Model_Resource_Db_Abstract
    {
    }
} else {
    abstract class AW_Betterthankyoupage_Model_Resource_RuleCommon extends Mage_Core_Model_Mysql4_Abstract
    {
        protected function _beforeSave(Mage_Core_Model_Abstract $object)
        {
            $from = $object->getData('from_date');
            if ($from instanceof Zend_Date) {
                $fromStr = $from->toString(Varien_Date::DATE_INTERNAL_FORMAT);
                $object->setData('from_date', $fromStr);
            }
            $to = $object->getData('to_date');
            if ($to instanceof Zend_Date) {
                $toStr = $to->toString(Varien_Date::DATE_INTERNAL_FORMAT);
                $object->setData('to_date', $toStr);
            }
        }
    }
}
