<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Betterthankyoupage_Adminhtml_RuleController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this
            ->_title($this->__('Promotions'))
            ->_title($this->__('Better Thank You Page Rules'));
        $this
            ->loadLayout()
            ->_setActiveMenu('promo')
            ->renderLayout()
        ;
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this
            ->_title($this->__('Promotions'))
            ->_title($this->__('Better Thank You Page Rules'));
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('betterthankyoupage/rule');
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('betterthankyoupage')->__('This rule no longer exists')
                );
                return $this->_redirect('*/*');
            }
            else {
                $this->_title($model->getTitle());
            }
        }
        else {
            $this->_title($this->__('New Rule'));
        }

        $data = Mage::getSingleton('adminhtml/session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        $model->getConditions()->setJsFormObject('rule_conditions_fieldset');

        Mage::register('betterthankyoupage_rule', $model);

        $this->loadLayout()
            ->_setActiveMenu('betterthankyoupage');

        $block = $this->getLayout()
            ->createBlock('betterthankyoupage/adminhtml_rule_edit')
            ->setData('action', $this->getUrl('*/betterthankyoupage_rule/save'));

        $this->getLayout()->getBlock('head')
            ->setCanLoadExtJs(true)
            ->setCanLoadRulesJs(true);

        $this
            ->_addContent($block)
            ->_addLeft($this->getLayout()->createBlock('betterthankyoupage/adminhtml_rule_edit_tabs'))
            ->renderLayout();
        return $this;
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $session = Mage::getSingleton('adminhtml/session');
                /**
                 * @var $model AW_Betterthankyoupage_Model_Rule
                 */
                $model = Mage::getModel('betterthankyoupage/rule');
                $data['conditions'] = $data['rule']['conditions'];
                unset($data['rule']);
                $data = $this->_filterDates($data, array('from_date', 'to_date'));

                $validateResult = $model->validateData(new Varien_Object($data));
                if ($validateResult !== true) {
                    foreach($validateResult as $errorMessage) {
                        $session->addError($errorMessage);
                    }
                    $session->setPageData($data);
                    return $this->_redirect('*/*/edit',
                        array(
                            'id'  => $model->getRuleId(),
                            'active_tab' => $this->getRequest()->getParam('tab')
                        )
                    );
                }

                $model->loadPost($data)->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('betterthankyoupage')->__('Rule was successfully saved')
                );
                $session->setPageData(false);

                if ($this->getRequest()->getParam('back')) {
                    return $this->_redirect('*/*/edit',
                        array(
                            'id'  => $model->getId(),
                            'active_tab' => $this->getRequest()->getParam('tab')
                        )
                    );
                }
                return $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('rule_id')));
                return $this;
            }
        }

        return $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                Mage::getModel('betterthankyoupage/rule')->load($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('catalogrule')->__('Rule was successfully deleted')
                );
                return $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                return $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('catalogrule')->__('Unable to find a page to delete')
        );
        return $this->_redirect('*/*/');
    }

    public function newConditionHtmlAction()
    {
        $id = $this->getRequest()->getParam('id');
        $typeArr = explode('|', str_replace('-', '/', $this->getRequest()->getParam('type')));
        $type = $typeArr[0];

        $model = Mage::getModel($type)
                ->setId($id)
                ->setType($type)
                ->setRule(Mage::getModel('betterthankyoupage/rule'))
                ->setPrefix('conditions');
        if (!empty($typeArr[1])) {
            $model->setAttribute($typeArr[1]);
        }

        if ($model instanceof Mage_Rule_Model_Condition_Abstract) {
            $model->setJsFormObject($this->getRequest()->getParam('form'));
            $html = $model->asHtmlRecursive();
        } else {
            $html = '';
        }
        $this->getResponse()->setBody($html);
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('betterthankyoupage/rule');
    }

}
