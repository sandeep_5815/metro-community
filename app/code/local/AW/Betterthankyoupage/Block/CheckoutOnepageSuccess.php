<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


/**
 *
 */
class AW_Betterthankyoupage_Block_CheckoutOnepageSuccess
    extends AW_Betterthankyoupage_Block_CheckoutOnepageSuccessCommon
{

    /**
     *
     */
    private $__sectionBlocks = array();
    private $__sortIndex     = array();
    /**
     * @var AW_Betterthankyoupage_Model_Rule
     */
    private $__rule = null;

    /**
     *
     */
    public function addSectionBlock($blockAlias, $sortOrder = '') {
        $__block = $this->getLayout()->getBlock($blockAlias);

        if ( $__block ) {
            if ( $sortOrder ) $__block->setSortOrder($sortOrder);

            array_push(
                $this->__sectionBlocks,
                $__block
            );

            $__sortOrder = $__block->getSortOrder();
            if ( !isset($this->__sortIndex[$__sortOrder]) ) $this->__sortIndex[$__sortOrder] = array();
            array_push($this->__sortIndex[$__sortOrder], $__block);
        }

        return $this;
    }

    public function addSection($blockAlias, $sortOrder) {
        return $this->addSectionBlock($blockAlias, $sortOrder);
    }

    /**
     *
     */
    public function getSectionBlocks() {
        return $this->__sectionBlocks;
    }

    public function getSections() {
        return $this->getSectionBlocks();
    }

    /**
     *
     */
    public function getSortedSectionBlocks() {
        $__index = $this->__sortIndex;
        ksort($__index);

        $__sortedBlocks = array();
        foreach ( $__index as $__blocks ) {
            foreach ( $__blocks as $__block ) {
                array_push($__sortedBlocks, $__block);
            }
        }

        return $__sortedBlocks;
    }

    public function getSortedSections() {
        return $this->getSortedSectionBlocks();
    }

    /**
     *
     */
    protected function _beforeToHtml() {
        $__result = parent::_beforeToHtml();
        if ( $this->isModuleEnabled() ) {
            /* AW_Sarp2 Compatibility */
            if (Mage::helper('betterthankyoupage')->isSarp2Installed()) {
                Mage::helper('betterthankyoupage/sarp2')->addSectionListToSuccessPage($this);
            }
            $this->setTemplate('betterthankyoupage/onepage_success.phtml');
        }
        return $__result;
    }

    /**
     *
     */
    public function isModuleEnabled() {
        return ( Mage::getStoreConfig('betterthankyoupage/general/module_enabled') ? true : false );
    }

    /**
     * @return AW_Betterthankyoupage_Model_Rule
     */
    public function getRule()
    {
        if (is_null($this->__rule)) {
            $this->_fetchRule();
        }
        return $this->__rule;
    }

    public function getOrderIds()
    {
        if ($orderId = $this->_getLastSingleOrderIdFromSession()) {
            return array($orderId);
        }
        if ($orderIds = $this->_getLastMultipleOrderIdsFromSession()) {
            return $orderIds;
        }
        return array();
    }

    protected function _getLastSingleOrderIdFromSession() {
        return Mage::getSingleton('checkout/session')->getLastOrderId();
    }

    protected function _getLastMultipleOrderIdsFromSession() {
        return Mage::getSingleton('core/session')->getOrderIds();
    }

    /**
     * @return int
     */
    protected function _getCustomerGroup()
    {
        /**
         * @var $customer Mage_Customer_Model_Session
         */
        $customer = Mage::getSingleton('customer/session');
        if ($customer->isLoggedIn()) {
            return $customer->getCustomer()->getGroupId();
        }
        return Mage_Customer_Model_Group::NOT_LOGGED_IN_ID;
    }

    protected function _fetchRule()
    {
        $this->__rule = Mage::getModel('betterthankyoupage/rule');
        $found = false;
        /**
         * @var $date Zend_Date
         */
        $date = new Zend_Date();
        /**
         * @var $collection AW_Betterthankyoupage_Model_Resource_Rule_Collection
         */
        $collection = $this->__rule->getCollection();
        $collection
            ->setActiveFilter()
            ->setDateFilter($date->now())
            ->setStoreFilter(Mage::app()->getStore()->getId())
            ->setCustomerGroupFilter($this->_getCustomerGroup())
            ->setPriorityOrder();

        /**
         * @var $item AW_Betterthankyoupage_Model_Rule
         */
        foreach ($collection->getItems() as $item) {
            # fix for older versions - reload the rule forcefully to expand conditions
            if (!Mage::helper('betterthankyoupage')->isMageVersionFit()) {
                $item->load($item->getId());
            }
            /**
             * @var $order Mage_Sales_Model_Order
             * @var $quote Mage_Sales_Model_Quote
             */
            foreach ($this->getOrderIds() as $orderId) {
                $order = Mage::getModel('sales/order');
                $order->load($orderId);
                if (!$order->getId()) {
                    $order->loadByIncrementId($orderId);
                }
                if (!$order->getId()) {
                    continue;
                }
                $quote = Mage::getModel('sales/quote');
                $quote->setStoreId($order->getStoreId())->load($order->getQuoteId());
                if (!$quote->getId()) {
                    continue;
                }
                $quote->collectTotals();
                $order->setQuote($quote);
                if ($item->validate($order)) {
                    $this->__rule = $item;
                    $found = true;
                    break;
                }
            }
            if ($found) {
                break;
            }
        }

        return $this->__rule;
    }
}
