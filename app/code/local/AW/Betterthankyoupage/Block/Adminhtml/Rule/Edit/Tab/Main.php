<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Betterthankyoupage_Block_Adminhtml_Rule_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
{

    const DEFAULT_PRIORITY_VALUE = 0;

    protected function _prepareForm()
    {
        $model = Mage::registry('betterthankyoupage_rule');
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            array('legend' => $this->__('General Information'))
        );

        if ($model->getId()) {
            $fieldset->addField(
                'rule_id',
                'hidden', array('name' => 'rule_id')
            );
        }

        $fieldset->addField('title', 'text', array(
            'name' => 'title',
            'label' => $this->__('Rule Name'),
            'title' => $this->__('Rule Name'),
            'required' => true,
        ));

        $fieldset->addField('description', 'textarea', array(
            'name' => 'description',
            'label' => $this->__('Description'),
            'title' => $this->__('Description'),
            'style' => 'height: 100px;',
        ));

        $fieldset->addField('is_active', 'select', array(
            'label' => $this->__('Status'),
            'title' => $this->__('Status'),
            'name' => 'is_active',
            'options' => array(
                '1' => $this->__('Active'),
                '0' => $this->__('Inactive'),
            ),
        ));

        if (Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('store_ids', 'hidden', array(
                'name'      => 'store_ids[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        } else {
            $field = $fieldset->addField('store_ids', 'multiselect', array(
                'name'      => 'store_ids[]',
                'label'     => $this->__('Stores'),
                'title'     => $this->__('Stores'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
            if (
                Mage::helper('betterthankyoupage')->isMageVersionFit()
                && class_exists('Mage_Adminhtml_Block_Store_Switcher_Form_Renderer_Fieldset_Element')
            ) {
                $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
                $field->setRenderer($renderer);
            } else {
                $this->setChild('store_switcher',
                    $this->getLayout()->createBlock('adminhtml/store_switcher')
                        ->setUseConfirm(false)
                        ->setSwitchUrl($this->getUrl('*/*/*', array('store'=>null)))
                        ->setTemplate('report/store/switcher.phtml')
                );
            }
        }

        $customerGroups = Mage::getResourceModel('customer/group_collection')->load()->toOptionArray();
        $found = false;
        foreach ($customerGroups as $group) {
            if ($group['value']==0) {
                $found = true;
            }
        }
        if (!$found) {
            array_unshift($customerGroups, array(
                'value' => 0,
                'label' => Mage::helper('salesrule')->__('NOT LOGGED IN'))
            );
        }

        $fieldset->addField('customer_group_ids', 'multiselect', array(
            'name'      => 'customer_group_ids[]',
            'label'     => $this->__('Customer Groups'),
            'title'     => $this->__('Customer Groups'),
            'required'  => true,
            'values'    => Mage::getResourceModel('customer/group_collection')->toOptionArray(),
        ));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $fieldset->addField('from_date', 'date', array(
            'name'   => 'from_date',
            'label'  => $this->__('From Date'),
            'title'  => $this->__('From Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso
        ));

        $fieldset->addField('to_date', 'date', array(
            'name'   => 'to_date',
            'label'  => $this->__('To Date'),
            'title'  => $this->__('To Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso
        ));

        $fieldset->addField('priority', 'text', array(
            'name' => 'priority',
            'class' => 'validate-digits',
            'label' => $this->__('Priority'),
            'title' => $this->__('Priority'),
            'note' => $this->__('Only one rule with greater priority will be processed')
        ));

        $form->setValues($this->_prepareFormValues($model->getData()));
        $this->setForm($form);
        return parent::_prepareForm();
    }

    protected function _prepareFormValues($values)
    {
        if (
            !array_key_exists('priority', $values)
            || empty($values['priority'])
        ) {
            $values['priority'] = self::DEFAULT_PRIORITY_VALUE;
        }

        return $values;
    }
}
