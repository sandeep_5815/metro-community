<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Betterthankyoupage_Block_Adminhtml_Rule_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_rule';
        $this->_blockGroup = 'betterthankyoupage';

        parent::__construct();

        $this->_updateButton('save', 'label', $this->__('Save'));
        $this->_updateButton('delete', 'label', $this->__('Delete'));

        $this->_addButton('save_and_continue_edit', array(
            'class'   => 'save',
            'label'   => $this->__('Save and Continue Edit'),
            'onclick' => 'saveAndContinueEdit(\'' . $this->_getSaveAndContinueUrl() . '\')',
        ), 10);

        $this->_formScripts[] = "
            function saveAndContinueEdit(url) {
                editForm.submit(url.replace(/{{tab_id}}/ig,betterthankyoupage_rule_tabsJsTabs.activeTab.id));
            }
        ";
    }

    public function getHeaderText()
    {
        $rule = Mage::registry('betterthankyoupage_rule');
        if ($rule->getRuleId()) {
            return $this->__("Edit Rule '%s'", $this->escapeHtml($rule->getTitle()));
        } else {
            return $this->__('New Rule');
        }
    }

    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl(
            '*/*/save',
            array(
                 '_current' => true,
                 'back'     => 'edit',
                 'tab'      => '{{tab_id}}'
            )
        );
    }

}
