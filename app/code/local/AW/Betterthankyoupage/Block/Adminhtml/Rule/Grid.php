<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Betterthankyoupage_Block_Adminhtml_Rule_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('rule_id');
        $this->setDefaultSort('is_active');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('betterthankyoupage/rule')->getResourceCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'rule_id',
            array(
                'header' => $this->__('ID'),
                'align' => 'right',
                'width' => '50px',
                'index' => 'rule_id',
            )
        );

        $this->addColumn(
            'title',
            array(
                'header' => $this->__('Rule Name'),
                'align' => 'left',
                'index' => 'title',
            )
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn(
                'store_ids',
                array(
                     'header'                    => $this->__('Store View'),
                     'index'                     => 'store_ids',
                     'type'                      => 'store',
                     'width'                     => '200px',
                     'store_all'                 => true,
                     'store_view'                => true,
                     'sortable'                  => false,
                     'renderer'                  => 'betterthankyoupage/adminhtml_rule_grid_renderer_multiStores',
                     'filter_condition_callback' => array($this, '_filterStore'),
                )
            );
        }

        $this->addColumn(
            'from_date',
            array(
                 'header' => $this->__('Date Start'),
                 'align'  => 'left',
                 'width'  => '120px',
                 'type'   => 'date',
                 'default' => '--',
                 'index'  => 'from_date',
            )
        );

        $this->addColumn(
            'to_date',
            array(
                 'header'  => $this->__('Date Expire'),
                 'align'   => 'left',
                 'width'   => '120px',
                 'type'    => 'date',
                 'default' => '--',
                 'index'   => 'to_date',
            )
        );

        $this->addColumn(
            'is_active',
            array(
                'header' => $this->__('Status'),
                'align' => 'left',
                'width' => '50px',
                'index' => 'is_active',
                'type' => 'options',
                'options' => array(
                    1 => $this->__('Active'),
                    0 => $this->__('Inactive'),
                ),
            )
        );

        $this->addColumn(
            'priority',
            array(
                'header' => $this->__('Priority'),
                'align' => 'right',
                'width' => '30px',
                'index' => 'priority',
            )
        );

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getRuleId()));
    }

    /**
     * @param AW_Betterthankyoupage_Model_Resource_Rule_Collection $collection
     * @param Varien_Object $column
     * @return $this
     */
    protected function _filterStore($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $collection->setStoreFilter($value);
        return $this;
    }

    /**
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _setCollectionOrder($column)
    {
        /**
         * @var $collection AW_Betterthankyoupage_Model_Resource_Rule_Collection
         */
        $collection = $this->getCollection();
        if ($collection) {
            switch($column->getId()) {
                case "is_active" :
                    $columnIndex = $column->getFilterIndex() ? $column->getFilterIndex() : $column->getIndex();
                    $collection->addOrder($columnIndex, strtoupper($column->getDir()));
                    $collection->addOrder('priority', strtoupper($column->getDir()));
                    break;
                default:
                    return parent::_setCollectionOrder($column);
            }
        }
        return $this;
    }

}
