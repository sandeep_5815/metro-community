<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Betterthankyoupage_Block_Adminhtml_Rule_Edit_Tab_Actions extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('betterthankyoupage_rule');
        $form = new Varien_Data_Form();
        $htmlIdPrefix = 'rule_';
        $form->setHtmlIdPrefix($htmlIdPrefix);

        $orderinfo = $form->addFieldset(
            'orderinfo_fieldset',
            array('legend' => $this->__('Order Information Section'))
        );

        $orderinfo->addField('orderinfo_display', 'select', array(
            'name' => 'orderinfo_display',
            'label' => $this->__('Display Section'),
            'title' => $this->__('Display Section'),
            'options'   => array(
                '1' => Mage::helper('salesrule')->__('Yes'),
                '0' => Mage::helper('salesrule')->__('No'),
            ),
            'after_element_html' => $this->_getDefaultCheckbox(
                $htmlIdPrefix . 'orderinfo_display',
                'orderinfo_display'
            ),
        ));

        $orderinfo->addField('orderinfo_sort', 'text', array(
            'name' => 'orderinfo_sort',
            'label' => $this->__('Section Sort Order'),
            'title' => $this->__('Section Sort Order'),
            'class' => 'validate-digits',
            'after_element_html' => $this->_getDefaultCheckbox(
                $htmlIdPrefix . 'orderinfo_sort',
                'orderinfo_sort'
            ),
        ));

        $orderinfo->addField('orderinfo_text', 'textarea', array(
            'name' => 'orderinfo_text',
            'label' => $this->__('Text Message'),
            'title' => $this->__('Text Message'),
            'after_element_html' => $this->_getDefaultCheckbox(
                $htmlIdPrefix . 'orderinfo_text',
                'orderinfo_text'
            ),
        ));

        $cms = $form->addFieldset(
            'cms_fieldset',
            array('legend' => $this->__('CMS Block Section'))
        );

        $cms->addField('cms_display', 'select', array(
            'name' => 'cms_display',
            'label' => $this->__('Display Section'),
            'title' => $this->__('Display Section'),
            'options'   => array(
                '1' => Mage::helper('salesrule')->__('Yes'),
                '0' => Mage::helper('salesrule')->__('No'),
            ),
            'after_element_html' => $this->_getDefaultCheckbox(
                $htmlIdPrefix . 'cms_display',
                'cms_display'
            ),
        ));

        $cms->addField('cms_sort', 'text', array(
            'name' => 'cms_sort',
            'label' => $this->__('Section Sort Order'),
            'title' => $this->__('Section Sort Order'),
            'class' => 'validate-digits',
            'after_element_html' => $this->_getDefaultCheckbox(
                $htmlIdPrefix . 'cms_sort',
                'cms_sort'
            ),
        ));

        $cms->addField('cms_block_id', 'select', array(
            'name' => 'cms_block_id',
            'label' => $this->__('CMS Block'),
            'title' => $this->__('CMS Block'),
            'values'   => Mage::getModel('betterthankyoupage/adminhtmlSystemConfigSourceCmsBlock')->toOptionArray(),
            'after_element_html' => $this->_getDefaultCheckbox(
                $htmlIdPrefix . 'cms_block_id',
                'cms_block_id'
            ),
        ));

        $form->setValues($this->_prepareFormValues($model->getData()));
        $this->setForm($form);
        return parent::_prepareForm();
    }

    protected function _getDefaultCheckbox($fieldId, $fieldName)
    {
        $model = Mage::registry('betterthankyoupage_rule');
        $configurationUrl = Mage::helper('adminhtml')->getUrl(
            'adminhtml/system_config/edit', array('section' => 'betterthankyoupage')
        );
        $afterElementHtml = '<div><input type="checkbox" id="use_config_' . $fieldId . '" '
            . 'name="use_config_' . $fieldName . '" value="1"'
            . 'onclick="toggleValueElements(this, this.parentNode.parentNode);">'
            . '<label for="use_config_' . $fieldId . '" class="normal">'
            .  $this->__("Use the option from <a href='%s'>Global Extension Settings</a>", $configurationUrl)
            . '</label></div>'
        ;
        if (
            is_null($model->getId())
            || is_null($model->getData($fieldName))
        ) {
            $afterElementHtml .= '<script type="text/javascript">'
                . 'Event.observe(window, "load", function(){'
                . '$("use_config_' . $fieldId . '").click();'
                . '});'
                . '</script>'
            ;
        }
        return $afterElementHtml;
    }

    protected function _prepareFormValues($values)
    {
        if (
            !array_key_exists('orderinfo_display', $values)
            || is_null($values['orderinfo_display'])
        ) {
            $values['orderinfo_display'] = Mage::getStoreConfig('betterthankyoupage/order_information_section/display_section');
        }
        if (
            !array_key_exists('orderinfo_sort', $values)
            || is_null($values['orderinfo_sort'])
        ) {
            $values['orderinfo_sort'] = Mage::getStoreConfig('betterthankyoupage/order_information_section/section_sort_order');
        }
        if (
            !array_key_exists('orderinfo_text', $values)
            || is_null($values['orderinfo_text'])
        ) {
            $values['orderinfo_text'] = Mage::getStoreConfig('betterthankyoupage/order_information_section/text_message');
        }
        if (
            !array_key_exists('cms_display', $values)
            || is_null($values['cms_display'])
        ) {
            $values['cms_display'] = Mage::getStoreConfig('betterthankyoupage/cms_block_section/display_section');
        }
        if (
            !array_key_exists('cms_sort', $values)
            || is_null($values['cms_sort'])
        ) {
            $values['cms_sort'] = Mage::getStoreConfig('betterthankyoupage/cms_block_section/section_sort_order');
        }
        if (
            !array_key_exists('cms_block_id', $values)
            || is_null($values['cms_block_id'])
        ) {
            $values['cms_block_id'] = Mage::getStoreConfig('betterthankyoupage/cms_block_section/cms_block');
        }
        return $values;
    }

}
