<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Betterthankyoupage
 * @version    1.1.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Betterthankyoupage_Block_Adminhtml_Rule_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('betterthankyoupage_rule_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Better Thank You Page Rules'));
    }

    protected function _beforeToHtml()
    {
        $mainSectionContent = $this->getLayout()
            ->createBlock('betterthankyoupage/adminhtml_rule_edit_tab_main')
            ->toHtml()
        ;
        $this->addTab(
            'main_section',
            array(
                'label' => $this->__('General'),
                'title' => $this->__('General'),
                'content' => $mainSectionContent,
                'active' => true
            )
        );

        $conditionsSectionContent = $this->getLayout()
            ->createBlock('betterthankyoupage/adminhtml_rule_edit_tab_conditions')
            ->toHtml()
        ;
        $this->addTab(
            'conditions_section',
            array(
                'label' => $this->__('Conditions'),
                'title' => $this->__('Conditions'),
                'content' => $conditionsSectionContent,
            )
        );

        $actionsSectionContent = $this->getLayout()
            ->createBlock('betterthankyoupage/adminhtml_rule_edit_tab_actions')
            ->toHtml()
        ;
        $this->addTab(
            'actions_section',
            array(
                'label' => $this->__('Actions'),
                'title' => $this->__('Actions'),
                'content' => $actionsSectionContent,
            )
        );
        return parent::_beforeToHtml();
    }

    public function getTabId($tab, $withPrefix = true)
    {
        return $tab->getId();
    }

}
