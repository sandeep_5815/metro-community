<?php
/**
 * InstantSearchPlus (Autosuggest)

 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Mage
 * @package    InstantSearchPlus
 * @copyright  Copyright (c) 2014 Fast Simon (http://www.instantsearchplus.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$helper=Mage::helper('autocompleteplus_autosuggest');

$keyOld=$helper->getKey();

Mage::log('mysql4-upgrade-2.0.2.4-2.0.2.5.php',null,'autocompleteplus.log');
//getting site url
$url=$helper->getConfigDataByFullPath('web/unsecure/base_url');

//getting site owner email
$storeMail=$helper->getConfigDataByFullPath('autocompleteplus/config/store_email');

//getting site design theme package name
$package=$helper->getConfigDataByFullPath('design/package/name');



$collection=Mage::getModel('catalog/product')->getCollection();
//$productCount=$collection->count();


$multistoreJson=$helper->getMultiStoreDataJson();

try{

    $commandOrig="http://0-1vk.acp-magento.appspot.com/install";

    $data=array();
    $data['multistore']=$multistoreJson;
    $data['f'] = '24';

    $key=$helper->sendPostCurl($commandOrig,$data);

    if(strlen($key)>50){
        $key='InstallFailedUUID';
    }

    Mage::log(print_r($key,true),null,'autocomplete.log');

    $errMsg='';
    if($key=='InstallFailedUUID'){
        $errMsg.='Could not get license string.';
    }

    if($package=='base'){
        $errMsg.= ';The Admin needs to move autocomplete template files to his template folder';
    }

    if($errMsg!=''){

        $command="http://0-1vk.acp-magento.appspot.com/install_error";
        $data=array();
        $data['site']=$url;
        $data['msg']=$errMsg;
        $data['email']=$storeMail;
        //$data['product_count']=$productCount;
        $data['multistore']=$multistoreJson;
        $data['f'] = '24';
        $res=$helper->sendPostCurl($command,$data);
    }

}catch(Exception $e){

    $key='failed';
    $errMsg=$e->getMessage();

    Mage::log('Install failed with a message: '.$errMsg,null,'autocomplete.log');

    $command="http://0-1vk.acp-magento.appspot.com/install_error";

    $data=array();
    $data['site']=$url;
    $data['msg']=$errMsg;
    $data['original_install_URL']=$commandOrig;
    $data['f'] = '24';
    $res=$helper->sendPostCurl($command,$data);
}

$installer = $this;

$installer->startSetup();

$res=$installer->run("UPDATE {$this->getTable('autocompleteplus_config')} SET licensekey='".$key."' WHERE id=1;");

$installer->endSetup();


?>