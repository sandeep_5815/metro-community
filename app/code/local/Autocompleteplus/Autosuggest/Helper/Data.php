<?php
/**
 * InstantSearchPlus (Autosuggest)

 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Mage
 * @package    InstantSearchPlus
 * @copyright  Copyright (c) 2014 Fast Simon (http://www.instantsearchplus.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Autocompleteplus_Autosuggest_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getConfigDataByFullPath($path){

       

    }

    public function getConfigMultiDataByFullPath($path){

        

    }

    public function sendCurl($command){

        if(isset($ch)) unset($ch);

        if(function_exists('curl_setopt')){
            $ch              = curl_init();
            curl_setopt($ch, CURLOPT_URL, $command);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
            $str=curl_exec($ch);
        }else{
            $str='failed';
        }

        return $str;


    }

    public function getKey(){

    }


    public static function sendPostCurl($command, $data=array(),$cookie_file='genCookie.txt') {

        if(isset($ch)) unset($ch);

        if(function_exists('curl_setopt')){

            $ch              = curl_init();
            curl_setopt($ch, CURLOPT_URL, $command);
            curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20100101 Firefox/21.0');
            //curl_setopt($ch,CURLOPT_POST,0);
            if(!empty($data)){
                curl_setopt_array($ch, array(
                    CURLOPT_POSTFIELDS => $data,
                ));
            }


            //  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            //      'Connection: Keep-Alive',
            //      'Keep-Alive: 800'
            //  ));

            $str=curl_exec($ch);

        }else{
            $str='failed';
        }

        return $str;
    }

    public function prepareGroupedProductPrice($groupedProduct)
    {
        $aProductIds = $groupedProduct->getTypeInstance()->getChildrenIds($groupedProduct->getId());

        $prices = array();
        foreach ($aProductIds as $ids) {
            foreach ($ids as $id) {
                $aProduct = Mage::getModel('catalog/product')->load($id);
                $prices[] = $aProduct->getPriceModel()->getPrice($aProduct);
            }
        }

        krsort($prices);
        $groupedProduct->setPrice($prices[0]);

        // or you can return price
    }

    public function getBundlePrice($product) {

        $optionCol= $product->getTypeInstance(true)
            ->getOptionsCollection($product);
        $selectionCol= $product->getTypeInstance(true)
            ->getSelectionsCollection(
                $product->getTypeInstance(true)->getOptionsIds($product),
                $product
            );
        $optionCol->appendSelections($selectionCol);
        $price = $product->getPrice();

        foreach ($optionCol as $option) {
            if($option->required) {
                $selections = $option->getSelections();
                $selPricesArr=array();

                if(is_array($selections)){

                    foreach($selections as $s){
                        $selPricesArr[]=$s->price;
                    }



                    $minPrice = min($selPricesArr);

                    if($product->getSpecialPrice() > 0) {
                        $minPrice *= $product->getSpecialPrice()/100;
                    }

                    $price += round($minPrice,2);

                }
            }
        }
        return $price;

    }

    public function getMultiStoreDataJson(){

        $websites=Mage::getModel('core/website')->getCollection();

        $multistoreData=array();
        $multistoreJson='';
        $useStoreCode=$this->getConfigDataByFullPath('web/url/use_store');
        $mage=Mage::getVersion();
        $ext=(string) Mage::getConfig()->getNode()->modules->Autocompleteplus_Autosuggest->version;
        $version=array('mage'=>$mage,'ext'=>$ext);

        //getting site url
        $url=$this->getConfigDataByFullPath('web/unsecure/base_url');

        //getting site owner email
        $storeMail=$this->getConfigDataByFullPath('autocompleteplus/config/store_email');

        if(!$storeMail){

            $storeMail=$this->getConfigDataByFullPath('trans_email/ident_general/email');
        }

        $collection=Mage::getModel('catalog/product')->getCollection();
        //$productCount=$collection->count();


        $storesArr=array();
        foreach($websites as $website){
            $code=$website->getCode();
            $stores=$website->getStores();
            foreach($stores as $store){
                $storesArr[$store->getStoreId()]=$store->getData();
            }
        }

        if(count($storesArr)==1){

            $dataArr=array(
                'stores'=>$multistoreData,
                'version'=>$version
            );

            $dataArr['site']=$url;
            $dataArr['email']=$storeMail;

            $multistoreJson=json_encode($dataArr);

        }else{

            $storeUrls=$this->getConfigMultiDataByFullPath('web/unsecure/base_url');
            $locales=$this->getConfigMultiDataByFullPath('general/locale/code');
            $storeComplete=array();

            foreach($storesArr as $key=>$value){

                if(!$value['is_active']){
                    continue;
                }

                $storeComplete=$value;
                if(array_key_exists($key,$locales)){
                    $storeComplete['lang']=$locales[$key];
                }else{
                    $storeComplete['lang']=$locales[0];
                }

                if(array_key_exists($key,$storeUrls)){
                    $storeComplete['url']=$storeUrls[$key];
                }else{
                    $storeComplete['url']=$storeUrls[0];
                }

                if($useStoreCode){
                    $storeComplete['url']=$storeUrls[0].$value['code'];
                }

                $multistoreData[]=$storeComplete;
            }

            $dataArr=array(
                'stores'=>$multistoreData,
                'version'=>$version
            );

            $dataArr['site']=$url;
            $dataArr['email']=$storeMail;
            //$dataArr['product_count']=$productCount;

            $multistoreJson=json_encode($dataArr);

        }
        
    }

    
    public function getExtensionConflict($all_conflicts = false){
        $all_rewrite_classes = array();
        $node_type_list = array('model', 'helper', 'block');
        
        foreach ($node_type_list as $node_type){
            foreach (Mage::getConfig()->getNode('modules')->children() as $name => $module) {
                if ($module->codePool == 'core' || $module->active != 'true'){
                    continue;
                }
                $config_file_path = Mage::getConfig()->getModuleDir('etc', $name) . DS . 'config.xml';
                $config = new Varien_Simplexml_Config();
                $config->loadString('<config/>');
                $config->loadFile($config_file_path);
                $config->extend($config, true);
                
                $nodes = $config->getNode()->global->{$node_type . 's'};
                if (!$nodes)
                    continue;
                foreach($nodes->children() as $node_name => $config) {
                    if ($config->rewrite){  // there is rewrite for current config
                        foreach($config->rewrite->children() as $class_tag => $derived_class){
                            $base_class_name = $this->_getMageBaseClass($node_type, $node_name, $class_tag);
                            
                            $lead_derived_class = '';
                            $conf = Mage::getConfig()->getNode()->global->{$node_type . 's'}->{$node_name};
                            if (isset($conf->rewrite->$class_tag)){
                                $lead_derived_class = (string)$conf->rewrite->$class_tag;
                            }
                            if ($derived_class == ''){
                                $derived_class = $lead_derived_class;
                            }
                            
                            if (empty($all_rewrite_classes[$base_class_name])){
                                $all_rewrite_classes[$base_class_name] = array(
                                	'derived' => array((string)$derived_class),
                                    'lead'    => (string)$lead_derived_class,
                                    'tag'     => $class_tag,
                                    'name'    => array((string)$name)
                                );                      
                            }else{
                                array_push($all_rewrite_classes[$base_class_name]['derived'], (string)$derived_class);
                                array_push($all_rewrite_classes[$base_class_name]['name'], (string)$name);
                            } 
                        }
                    }
                }                
            }
        }
        if ($all_conflicts){
            return $all_rewrite_classes;
        }
        
        $isp_rewrite_classes = array();
        $isp_module_name = 'Autocompleteplus_Autosuggest';
        foreach ($all_rewrite_classes as $base => $conflict_info){
            if (in_array($isp_module_name, $conflict_info['name'])){        // if isp extension rewrite this base class
                if (count($conflict_info['derived']) > 1){                  // more then 1 class rewrite this base class => there is a conflict
                    $isp_rewrite_classes[$base] = $conflict_info;
                }
            }
        }
        return $isp_rewrite_classes;
    }
    
    protected function _getMageBaseClass($node_type, $node_name, $class_tag){
        $config = Mage::getConfig()->getNode()->global->{$node_type . 's'}->$node_name;
        
        if (!empty($config)) {
            $className = $config->getClassName();
        }
        if (empty($className)) {
            $className = 'mage_'.$node_name.'_'.$node_type;
        }
        if (!empty($class_tag)) {
            $className .= '_'.$class_tag;
        }
        return uc_words($className); 
    }
    
}