<?php

class Tm_Deliveryarea_Model_Mysql4_Deliveryarea extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the deliveryarea_id refers to the key field in your database table.
        $this->_init('deliveryarea/deliveryarea', 'deliveryarea_id');
    }
}