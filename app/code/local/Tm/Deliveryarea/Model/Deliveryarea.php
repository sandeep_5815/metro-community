<?php

class Tm_Deliveryarea_Model_Deliveryarea extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('deliveryarea/deliveryarea');
    }
}