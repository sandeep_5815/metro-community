<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('deliveryarea')};
CREATE TABLE {$this->getTable('deliveryarea')} (
  `deliveryarea_id` int(11) unsigned NOT NULL auto_increment,
  `city_name` varchar(255) NOT NULL default '',
  `state_name` varchar(255) NOT NULL default '',
  `city_pincode` varchar(255) NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`deliveryarea_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup();
