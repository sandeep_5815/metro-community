<?php

class Tm_Deliveryarea_Block_Adminhtml_Deliveryarea_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('deliveryareaGrid');
      $this->setDefaultSort('deliveryarea_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('deliveryarea/deliveryarea')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('deliveryarea_id', array(
          'header'    => Mage::helper('deliveryarea')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'deliveryarea_id',
      ));

      $this->addColumn('city_name', array(
          'header'    => Mage::helper('deliveryarea')->__('City Name'),
          'align'     =>'left',
          'index'     => 'city_name',
      ));
      $this->addColumn('state_name', array(
          'header'    => Mage::helper('deliveryarea')->__('State Name'),
          'align'     =>'left',
          'index'     => 'state_name',
      ));
       $this->addColumn('city_pincode', array(
          'header'    => Mage::helper('deliveryarea')->__('Pin Code'),
          'align'     =>'left',
          'index'     => 'city_pincode',
      ));

      $this->addColumn('status', array(
          'header'    => Mage::helper('deliveryarea')->__('COD'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('deliveryarea')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('deliveryarea')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('deliveryarea')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('deliveryarea')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('deliveryarea_id');
        $this->getMassactionBlock()->setFormFieldName('deliveryarea');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('deliveryarea')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('deliveryarea')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('deliveryarea/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('deliveryarea')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('deliveryarea')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}