<?php
class Tm_Deliveryarea_Block_Deliveryarea extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getDeliveryarea()     
     { 
        if (!$this->hasData('deliveryarea')) {
            $this->setData('deliveryarea', Mage::registry('deliveryarea'));
        }
        return $this->getData('deliveryarea');
        
    }
}