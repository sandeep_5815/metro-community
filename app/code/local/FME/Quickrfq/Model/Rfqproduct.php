<?php

/**
 * @category  TM
 * @package   TM_Quickrfq
 * @author    Anjan Barik<AB00350686@techmahindra.com>
 * @copyright Anjan Barik<AB00350686@techmahindra.com>
 * 
 */
class FME_Quickrfq_Model_Rfqproduct extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('quickrfq/rfqproduct');
    }
}
?>
