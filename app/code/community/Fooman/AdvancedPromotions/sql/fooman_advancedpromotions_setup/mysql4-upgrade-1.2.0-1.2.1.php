<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_item'), 'fooman_free_qty', "int(10)");
$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_item'), 'fooman_applied_rule_ids', "text");
$installer->endSetup();