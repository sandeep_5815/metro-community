<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()->addColumn($installer->getTable('sales_flat_quote_item'), 'fooman_auto_added_qty', "int(10)");

//reverse dropping of the index
if(version_compare(Mage::getVersion(),'1.4.1.0') >= 0){
    try {
        $installer->run("                
            ALTER TABLE {$this->getTable('salesrule_coupon')} ADD UNIQUE `UNQ_COUPON_CODE` (`code`);
        ");
    } catch (Exception $e) {
        Mage::logException($e);
    }
}

$installer->endSetup();