<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_Sales_Mysql4_Quote_Item_Collection extends Mage_Sales_Model_Mysql4_Quote_Item_Collection
{

    public function getAutoAddedQtyForQuote ($quote)
    {
        $total = 0;
        foreach ($quote->getItemsCollection() as $item) {
            if (!$item->isDeleted()) {
                $total += $item->getFoomanAutoAddedQty();
            }
        }
        return $total > 0 ? $total : 0;
    }
}