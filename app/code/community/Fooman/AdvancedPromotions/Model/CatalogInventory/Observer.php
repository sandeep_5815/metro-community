<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_CatalogInventory_Observer extends Mage_CatalogInventory_Model_Observer
{

    public function checkQuoteItemQty($observer)
    {
        $quoteItem = $observer->getEvent()->getItem();
        if(!$quoteItem->getId() && $quoteItem->getOptionByCode('fooman_advancedpromotions_auto_added')) { 
            $this->_getProductQtyForCheck($quoteItem->getProductId(), -1 * $quoteItem->getQtyToAdd());
        }      
        return parent::checkQuoteItemQty($observer);
    }

}