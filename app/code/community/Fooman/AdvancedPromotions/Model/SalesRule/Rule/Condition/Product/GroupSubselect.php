<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_GroupSubselect
    extends Mage_SalesRule_Model_Rule_Condition_Product_Subselect
{
    const TYPE = 'fooman_advancedpromotions/salesRule_rule_condition_product_groupSubselect';

    public function loadOperatorOptions ()
    {
        parent::loadOperatorOptions();
        $extraOption = array('>=' => Mage::helper('fooman_advancedpromotions')->__('every'));
        $this->setOperatorOption($extraOption);
        return $this;
    }

    public function loadAttributeOptions ()
    {
        $this->setAttributeOption(array(
                'qty' => Mage::helper('salesrule')->__('For')
            ));
        return $this;
    }

    public function asHtml ()
    {
        $html = $this->getTypeElement()->getHtml() .
            Mage::helper('fooman_advancedpromotions')->__("%s %s %s items in cart matching these conditions:",
                $this->getAttributeElement()->getHtml(),
                $this->getOperatorElement()->getHtml(),
                $this->getValueElement()->getHtml()
            );
        if ($this->getId() != '1') {
            $html.= $this->getRemoveLinkHtml();
        }
        return $html;
    }

    public function getType ()
    {
        return self::TYPE;
    }


    public function getValueElementType()
    {
        return 'text';
    }

    public function validateItems (
        Varien_Object $quote,
        $address,
        $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
        $groupBy = false,
        $multiplier = 1,
        $groupIdentifier = 'simple',
        $conditionsMatched = array(),
        $attrValue = ''
    )
    {

        $matches = array();
        $tmpMatches = array();
        $all = $this->getAggregator() === 'all';
        $true = (bool) $this->getValue();
        $found = false;
        $ruleMatchesXtimes = array();
        $ruleType='';
        $nrConditions = 0;

        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;
        if ($this->getAttribute() == 'qty') {
            $multiplier = $this->getValue();
        }

        foreach ($this->getConditions() as $cond) {
            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                Mage::helper('fooman_advancedpromotions')->debug($cond->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, $groupBy, 1, $groupIdentifier), Zend_Log::WARN, $returnFormat, 'GROUP SUBSELECT');
            }
            $tmpMatches[] = $cond->validateItems($quote, $address, $returnFormat, $groupBy, 1, $groupIdentifier);
            $nrConditions++;
        }

        if ($nrConditions == 0) {
            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $quoteItem, $groupBy, $groupIdentifier, $multiplier, $attrValue, $debug);
            }
        } else {
            //combine the different matches into 1 group
            $tmpMatches = Mage::helper('fooman_advancedpromotions')->mergeTmpMatches($tmpMatches);

            foreach ($tmpMatches['matches'] as $attrKey => $groupMatched) {
                foreach ($groupMatched as $attrValue => $items) {
                    $groupMatchesXtimes = array();
                    foreach ($items as $key=>$item) {
                        if (!isset($groupMatchesXtimes[$key])) {
                            $groupMatchesXtimes[$key] = $item['rule_qty'];
                        } else {
                            $groupMatchesXtimes[$key] = min($item['rule_qty'], $groupMatchesXtimes[$key]);
                        }
                        $item['rule_qty'] = $groupMatchesXtimes[$key];
                        $matches[$attrKey][$attrValue][$key]= $item;
                    }
                    $summedMatches = 0;
                    foreach ($groupMatchesXtimes as $ruleMatch) {
                        $summedMatches += $ruleMatch;
                    }
                    if(isset($ruleMatchesXtimes[$attrKey][$attrValue])) {
                        $ruleMatchesXtimes[$attrKey][$attrValue] = min($ruleMatchesXtimes[$attrKey][$attrValue],floor($summedMatches/$multiplier));
                    } else {
                        $ruleMatchesXtimes[$attrKey][$attrValue] = floor($summedMatches/$multiplier);
                    }
                    Mage::helper('fooman_advancedpromotions')->saveAddRule($this->getRule()->getId(),$attrKey.$attrValue,$ruleMatchesXtimes[$attrKey][$attrValue] );
                }
            }
        }

        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
            Mage::helper('fooman_advancedpromotions')->debug($ruleMatchesXtimes, Zend_Log::NOTICE, $returnFormat, get_class($this) . ' rule matches X times');
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $this->getType(), $ruleMatchesXtimes, $matches);
    }

    /**
     * validate
     *
     * @param Varien_Object $object Quote
     * @return boolean
     */
    public function validate(Varien_Object $object)
    {
        if (substr($this->getRule()->getSimpleAction(), 0, 6) == 'super_') {
            return true;
        } else {
            return parent::validate($object);
        }
    }


}