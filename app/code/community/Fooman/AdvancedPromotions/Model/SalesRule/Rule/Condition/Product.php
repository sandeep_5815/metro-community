<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product extends Mage_SalesRule_Model_Rule_Condition_Product {

    public function validateItems (
            Varien_Object $quote,
            $address,            
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = '',
            $reverseAutoAdded = false
    )
    {
        $matches = array();
        $all = $this->getAggregator()==='all';
        $true = (bool)$this->getValue();
        $found = false;
        $ruleMatchesXtimes = 0;
        $ruleType = '';
        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;

        foreach ($quote->getAllItems() as $quoteItem) {
            if(!$quoteItem->isDeleted() && (!($reverseAutoAdded && $quoteItem->getOptionByCode('fooman_advancedpromotions_auto_added')))){
                $isSubProduct = false;
                $parentQuoteItemId = $quoteItem->getParentItemId();
                if ($parentQuoteItemId) {
                    $parentQuoteItem = $quoteItem->getParentItem();
                    if($parentQuoteItem) {
                        $parentProductType = $parentQuoteItem->getProductType();
                        if ( $parentProductType == 'configurable' || $parentProductType == 'bundle'){
                            $isSubProduct = true;
                        }
                    }
                }
                if ($this->validate($quoteItem)) {
                    //make sure if the subproduct validates to keep the parent as matching
                    //ie Magento can sometimes successfully match the subitem but not the parent
                    if($isSubProduct) {
                        $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $parentQuoteItem, $groupBy, $groupIdentifier, 1, $attrValue, $debug);
                    } else {
                        $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $quoteItem, $groupBy, $groupIdentifier, 1, $attrValue, $debug);
                    }
                } else {
                    if($returnFormat=='debug'){
                        //echo "NOT VALIDATED";
                    }

                }
            }
        }
        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
            Mage::helper('fooman_advancedpromotions')->debug(get_class($this).' rule matches X times: '.$ruleMatchesXtimes, Zend_Log::DEBUG, $returnFormat);
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $ruleType, $ruleMatchesXtimes, $matches);
    }
}
