<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Customer extends Mage_SalesRule_Model_Rule_Customer {

    public function validateItems (
            Varien_Object $quote,
            $address,            
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {
        $matches = array();
        $ruleMatchesXtimes = 0;

        if ($this->validate($quote)) {
            $ruleMatchesXtimes = 1;
            $matches[2] = $quote->getAllItems();
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, '', $ruleMatchesXtimes, $matches);
    }

}