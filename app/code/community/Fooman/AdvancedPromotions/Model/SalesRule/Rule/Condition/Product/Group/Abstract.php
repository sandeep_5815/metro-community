<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

abstract class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_Group_Abstract
    extends Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_Combine
{

    public function __construct()
    {
        parent::__construct();
        $this->setType($this->getType());
    }

    public function getType()
    {
        return '';
    }

    public function getNewChildSelectOptions()
    {
        $choices = array(
            array('value'=>'fooman_advancedpromotions/salesRule_rule_condition_product_groupSubselect', 'label'=>Mage::helper('fooman_advancedpromotions')->__('Products subselection')),
            array('value'=>'fooman_advancedpromotions/salesRule_rule_condition_product_groupSubselectAll', 'label'=>Mage::helper('fooman_advancedpromotions')->__('For X Products'))
        );
        return $choices;
    }

    public function loadValueOptions()
    {
        $productCondition = Mage::getModel('catalogrule/rule_condition_product');
        $productAttributes = $productCondition->loadAttributeOptions()->getAttributeOption();
        $attributes = array();        
        foreach ($productAttributes as $attributeCode=>$label) {
            $attributes[$attributeCode] = $label;
        }
        $this->setValueOption($attributes);
        return $this;
    }


    /**
     * validate
     *
     * @param Varien_Object $object Quote
     * @return boolean
     */
    public function validate(Varien_Object $object)
    {
        return true;
    }

    public function validateItems (
            Varien_Object $quote,
            $address,            
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {
        $matches = array();
        $ruleMatchesXtimes = 0;
        $nrConditions = 0;
        $ruleType=$this->getGroupMatchType();
     
        foreach ($this->getConditions() as $cond) {
            $tmpMatches=$cond->validateItems($quote, $address, $returnFormat, $this->getValue(), $multiplier);
            if ($tmpMatches) {
                if (is_array($tmpMatches)) {
                    foreach ($tmpMatches as $tmpMatch) {
                        if (is_array($tmpMatch)) {
                            foreach ($tmpMatch as $group) {
                                if (is_array($group)) {
                                    $matches = array_merge_recursive($matches,$group);
                                }
                            }
                        }
                    }
                } else {
                    $ruleMatchesXtimes = $tmpMatches;
                }                
            }
            $nrConditions ++;
        }

        $groupsCombined = array();
        foreach ($matches as $attr => $match) {
            foreach ($match as $attrValue => $group) {
                //all conditions in group are matched
                if (sizeof($group) == $nrConditions) {
                    $nrGroupMatched = 0;
                    foreach ($group as $itemMatched) {
                        if ($nrGroupMatched == 0) {
                            $nrGroupMatched = $itemMatched;
                        } else {
                            $nrGroupMatched = min($nrGroupMatched, $itemMatched);
                        }
                    }
                    
                    //if matching across category ids we can have matches for each category
                    //work out which category to use after looping over all matches
                    if ($attr == 'category_ids'){
                        $maxCatCombined[$attrValue] = $nrGroupMatched;
                    } else {
                        $groupsCombined[$attr][$attrValue] = $nrGroupMatched;
                        $ruleMatchesXtimes += $nrGroupMatched;
                    }
                }
            }
            //save category match
            if ($attr == 'category_ids') {
                asort($maxCatCombined, SORT_NUMERIC);
                $maxCatCombinedKeys = array_keys($maxCatCombined);
                //take the highest n matches / n= $nrConditions
                for ($i = $nrConditions; $i > 0; $i--) {
                    $currentMatchKey = array_pop($maxCatCombinedKeys);
                    $currentMatch = array($currentMatchKey => $maxCatCombined[$currentMatchKey]);
                    $ruleMatchesXtimes = $maxCatCombined[$currentMatchKey];
                }
                $groupsCombined['category_ids'] = $currentMatch;
            }
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $ruleType, $ruleMatchesXtimes, $groupsCombined);
    }

}