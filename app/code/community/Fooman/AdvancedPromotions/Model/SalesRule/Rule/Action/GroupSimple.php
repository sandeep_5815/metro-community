<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Action_GroupSimple
    extends Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_GroupSimple
{
    const TYPE='fooman_advancedpromotions/salesRule_rule_action_groupSimple';

    public function getType()
    {
        return self::TYPE;
    }

    public function loadValueOptions ()
    {
        $this->setValueOption(array(
            Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_SIMPLE => Mage::helper('fooman_advancedpromotions')->__('to every item in the group'),
            Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_MATCHED => Mage::helper('fooman_advancedpromotions')->__('for each group as matched under conditions')
        ));
        return $this;
    }

    public function getNewChildSelectOptions()
    {
        return $this->getOrigNewChildSelectOptions();
    }

    public function asHtml ()
    {
        $html = $this->getTypeElement()->getHtml() .
                Mage::helper('fooman_advancedpromotions')->__("Discount is applied %s", $this->getValueElement()->getHtml());
        if ($this->getId() != '1') {
            $html.= $this->getRemoveLinkHtml();
        }
        return $html;
    }

    public function validateItems (
            Varien_Object $quote,
            $address,
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {
        $matches = array();
        $tmpMatches = array();
        $groupMatchLookup = array();
        $ruleMatchesXtimes = 0;
        $nrConditions = 0;
        $ruleType=$this->getValue();
        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;

        switch ($ruleType) {
            default:
            case Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_SIMPLE:
                foreach ($this->getActions() as $action) {
                    if ($groupIdentifier == 'simple') {
                        $adjGroupIdentifier = $groupIdentifier . '-' . $action->getRule()->getId() . '|' . $action->getId();
                    } else {
                        $adjGroupIdentifier = $groupIdentifier . '|' . $action->getId();
                    }
                    if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                        Mage::helper('fooman_advancedpromotions')->debug($action->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, true, $multiplier, $adjGroupIdentifier), Zend_Log::ERR, $returnFormat,'GROUP SIMPLE ACTION 1');
                    }
                    $tmpMatches[]=$action->validateItems($quote, $address, $returnFormat, true, $multiplier, $adjGroupIdentifier);
                    $nrConditions ++;
                }
                break;
            case Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_MATCHED:
                foreach ($this->getActions() as $action) {
                    if($groupIdentifier=='simple'){
                        $groupIdentifier.= '-'.$action->getRule()->getId();
                    }
                    if(isset($conditionsMatched['matches']) && is_array($conditionsMatched['matches'])) {
                        foreach($conditionsMatched['matches'] as $attrKey=>$conditionAttrGroup) {
                            foreach($conditionAttrGroup as $attrValue=>$conditionMatches) {
                                if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                                    Mage::helper('fooman_advancedpromotions')->debug($action->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, $attrKey, $multiplier, $groupIdentifier, $conditionsMatched, $attrValue), Zend_Log::ERR, $returnFormat,'GROUP SIMPLE ACTION 2');
                                }
                                $tmpMatches[]=$action->validateItems($quote, $address, $returnFormat, $attrKey, $multiplier, $groupIdentifier, $conditionsMatched, $attrValue);
                                //tally up the matches for this group
                                $groupMatch = 0;
                                foreach ($conditionMatches as $conditionMatch) {
                                    $groupMatch += $conditionMatch['rule_qty'];
                                }
                                if(!isset($groupMatchLookup[$attrKey][$attrValue])) {
                                    $groupMatchLookup[$attrKey][$attrValue] = floor($groupMatch/$multiplier);
                                } else {
                                    $groupMatchLookup[$attrKey][$attrValue] = floor($groupMatch/$multiplier);
                                }
                            }
                        }
                    } else {
                        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                            Mage::helper('fooman_advancedpromotions')->debug($action->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, true, $multiplier, $groupIdentifier.'-'.$action->getRule()->getId()), Zend_Log::ERR, $returnFormat,'GROUP SIMPLE ACTION 3');
                        }
                        $tmpMatches[]=$action->validateItems($quote, $address, $returnFormat, true, $multiplier, $groupIdentifier.'-'.$action->getRule()->getId());
                    }
                    $nrConditions ++;
                }
                break;
        }

        if ($nrConditions == 0) {
            if($groupIdentifier=='simple'){
                $groupIdentifier.= '-'.$this->getRuleId();
            }
            //no conditions given default to matching all items
            switch ($ruleType) {
                default:
                case Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_SIMPLE:
                    foreach ($quote->getAllVisibleItems() as $quoteItem) {
                        $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $quoteItem, true, $groupIdentifier, $multiplier, $attrValue, $debug);
                    }
                    break;
                case Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_MATCHED:
                    if (isset($conditionsMatched['matches']) && is_array($conditionsMatched['matches'])) {
                        foreach ($conditionsMatched['matches'] as $attrKey => $conditionAttrGroup) {
                            foreach ($conditionAttrGroup as $attrValue => $conditionMatches) {
                                foreach ($quote->getAllVisibleItems() as $quoteItem) {
                                    $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $quoteItem, $attrKey, $groupIdentifier, $multiplier, $attrValue, $debug);
                                }
                            }
                        }
                    } else {
                        foreach ($quote->getAllVisibleItems() as $quoteItem) {
                            $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $quoteItem, $groupBy, $groupIdentifier, $multiplier, $attrValue, $debug);
                        }
                    }
                break;
            }
        } else {
            $groupsCombined = array();
            $nrGroupMatched = 0;
            //combine the different matches into 1 group
            foreach ($tmpMatches as $groupMatched) {
                if (isset($groupMatched['matches']) && $groupMatched['rule_qty']) {
                    $groupsCombined = array_merge_recursive($groupsCombined, $groupMatched['matches']);
                }
            }

            foreach ($groupsCombined as $attrKey => $groupMatched) {
                //remove the rule splitter
                if (strpos($attrKey,'|') > 0) {
                    $tmpAttrKey = current(explode('|',$attrKey));
                } else {
                    $tmpAttrKey = $attrKey;
                }

                foreach ($groupMatched as $attrValue => $items) {
                    if ($ruleType != Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_SIMPLE && sizeof($items) < $nrConditions) {
                        continue;
                    }
                    foreach ($items as $key => $item) {
                        if ($ruleType == Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_MATCHED) {
                            $item['rule_qty'] = $groupMatchLookup[$tmpAttrKey][$attrValue];
                        }
                        $ruleMatchesXtimes += $item['rule_qty'];
                        $matches[$attrKey][$attrValue][$key] = $item;
                    }
                }
            }
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $ruleType, $ruleMatchesXtimes, $matches);
    }


}