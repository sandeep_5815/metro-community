<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_GroupSubselectQty extends Mage_SalesRule_Model_Rule_Condition_Product_Subselect {

    const TYPE = 'fooman_advancedpromotions/salesRule_rule_condition_product_groupSubselectQty';

    /**
     * validate
     *
     * @param Varien_Object $object Quote
     * @return boolean
     */
    public function validate(Varien_Object $object)
    {
        if (substr($this->getRule()->getSimpleAction(), 0, 6) == 'super_') {
            return true;
        } else {
            return parent::validate($object);
        }
    }

    public function loadOperatorOptions()
    {
        parent::loadOperatorOptions();
        $extraOption = array('>='  =>Mage::helper('fooman_advancedpromotions')->__('every'));
        $this->setOperatorOption($extraOption);
        return $this;
    }

    public function loadAttributeOptions()
    {
        $this->setAttributeOption(array(
            'qty'  => Mage::helper('salesrule')->__('For')
        ));
        return $this;
    }

    public function asHtml()
    {
        $html = $this->getTypeElement()->getHtml().
            Mage::helper('fooman_advancedpromotions')->__("%s %s %s items in the cart",
              $this->getAttributeElement()->getHtml(),
              $this->getOperatorElement()->getHtml(),
              $this->getValueElement()->getHtml()
           );
           if ($this->getId()!='1') {
               $html.= $this->getRemoveLinkHtml();
           }
        return $html;
    }

    public function getType()
    {
        return self::TYPE;
    }
    
    public function getValueElementType()
    {
        return 'text';
    }    
    
    public function loadValueOptions()
    {
        $this->setValueOption(array());
        return $this;
    }    
    
    public function asHtmlRecursive()
    {
        return $this->asHtml();
    }

    public function validateItems (
            Varien_Object $quote,
            $address,            
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {

        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;

        $matches = array();
        $ruleMatchesXtimes = 0;
        $ruleMatchesXtimesArray = array();
        foreach ($quote->getAllVisibleItems() as $quoteItem) {
            $key = Mage::helper('fooman_advancedpromotions')->getItemKey($quoteItem);
            $ruleItemMatches = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, 0, $quoteItem, $groupBy, $groupIdentifier, $multiplier, $attrValue, $debug);
            //loop over items so each quote item can only contribute once to the overall result
            if (!isset($ruleMatchesXtimesArray[$key])) {
                $ruleMatchesXtimesArray[$key] = $ruleItemMatches;
            } else {
                $ruleMatchesXtimesArray[$key] = max($ruleItemMatches, $ruleMatchesXtimesArray[$key]);
            }
        }
        foreach ($ruleMatchesXtimesArray as $ruleMatch) {
            $ruleMatchesXtimes += $ruleMatch;
        }
        if ($this->getAttribute() == 'qty' && $this->getValue() > 0) {
            Mage::helper('fooman_advancedpromotions')->saveQtyRule( $this->getRule()->getId(), $this->getValue());
            $autoAdded = Mage::getModel('fooman_advancedpromotions/sales_mysql4_quote_item_collection')->getAutoAddedQtyForQuote($quote);
            Mage::helper('fooman_advancedpromotions')->debug('$autoAdded ' . $autoAdded, Zend_Log::NOTICE, $returnFormat);
            $ruleMatchesXtimes = floor(($ruleMatchesXtimes - $autoAdded) / $this->getValue());
        }

        if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
            Mage::helper('fooman_advancedpromotions')->debug(get_class($this).' rule matches X times: '.$ruleMatchesXtimes, Zend_Log::NOTICE, $returnFormat);
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $this->getType(), $ruleMatchesXtimes, $matches);
    }

}