<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_Found extends Mage_SalesRule_Model_Rule_Condition_Product_Found {

    public function validateItems (
            Varien_Object $quote,
            $address,            
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {
        $matches = array();
        $all = $this->getAggregator()==='all';
        $true = (bool)$this->getValue();
        $nrConditions = 0;
        $found = false;
        $ruleMatchesXtimes = 0;
        $ruleType='';

        if ($this->getAttribute() == 'qty') {
            $multiplier = $this->getValue();
        } else {
            $multiplier = 1;
        }

        foreach ($this->getConditions() as $cond) {
            $tmpMatches = $cond->validateItems($quote, $address, $returnFormat, $groupBy, $multiplier);
                if ($true) {
                    if(is_array($tmpMatches)) {
                        $matches = array_merge($matches, $tmpMatches);
                        if ($all) {
                            if ($ruleMatchesXtimes == 0) {
                                $ruleMatchesXtimes = floor($tmpMatches['rule_qty']/$multiplier);
                            } else {
                                $ruleMatchesXtimes = min(floor($tmpMatches['rule_qty']/$multiplier), $ruleMatchesXtimes);
                            }
                        } else {
                            $ruleMatchesXtimes = max(floor($tmpMatches['rule_qty']/$multiplier), $ruleMatchesXtimes);
                        }
                        if($tmpMatches['rule_type']){
                            $ruleType = $tmpMatches['rule_type'];
                        }
                    } else {
                        if ($all) {
                            if ($ruleMatchesXtimes == 0) {
                                $ruleMatchesXtimes = floor($tmpMatches/$multiplier);
                            } else {
                                $ruleMatchesXtimes = min(floor($tmpMatches/$multiplier), $ruleMatchesXtimes);
                            }
                        } else {
                            $ruleMatchesXtimes = max(floor($tmpMatches/$multiplier), $ruleMatchesXtimes);
                        }
                        if(!is_int($tmpMatches)){
                            $ruleMatchesXtimes = $tmpMatches;
                        }
                    }
                }
                $nrConditions++;
        }
        $ruleMatchesXtimes == 0 ? $found=false:$found=true;
        // no item found and no conditions so all items match and rule matched once
        if(!$found && $nrConditions==0) {
            $ruleMatchesXtimes = 1;
            $matches['matches'] = $quote->getAllItems();
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $this->getType(), $ruleMatchesXtimes, $matches);
    }

}