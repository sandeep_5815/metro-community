<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Action_GroupIndependant
    extends Fooman_AdvancedPromotions_Model_SalesRule_Rule_Condition_Product_GroupIndependant
{

    const TYPE = 'fooman_advancedpromotions/salesRule_rule_action_groupIndependant';

    public function getType()
    {
        return self::TYPE;
    }

    public function getNewChildSelectOptions()
    {
        return $this->getOrigNewChildSelectOptions();
    }

    public function loadValueOptions()
    {
        $productCondition = Mage::getModel('catalogrule/rule_condition_product');
        $productAttributes = $productCondition->loadAttributeOptions()->getAttributeOption();
        $attributes = array();
        foreach ($productAttributes as $attributeCode=>$label) {
            $attributes[$attributeCode] = $label;
        }
        $this->setValueOption($attributes);
        return $this;
    }

    public function loadOperatorOptions ()
    {
        $this->setOperatorOption(array(
            Fooman_AdvancedPromotions_Helper_Data::ACTION_GROUP_INDEPENDANT => 'for each group as matched by'
        ));
        return $this;
    }

    public function asHtml ()
    {
        $html = $this->getTypeElement()->getHtml() .
                Mage::helper('fooman_advancedpromotions')->__("Discount is applied %s %s",$this->getOperatorElement()->getHtml(), $this->getValueElement()->getHtml());
        if ($this->getId() != '1') {
            $html.= $this->getRemoveLinkHtml();
        }
        return $html;
    }

    public function validateItems (
            Varien_Object $quote,
            $address,
            $returnFormat = Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_WHAT_MATCHED,
            $groupBy = false,
            $multiplier = 1,
            $groupIdentifier = 'simple',
            $conditionsMatched = array(),
            $attrValue = ''
    )
    {
        $matches = array();
        $ruleMatchesXtimes = 0;
        $nrConditions = 0;
        $ruleType=$this->getGroupMatchType();
        $debug = $returnFormat == Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG;

        foreach ($this->getActions() as $action) {
            if (Mage::helper('fooman_advancedpromotions')->isDebugMode()) {
                Mage::helper('fooman_advancedpromotions')->debug($action->validateItems($quote, $address, Fooman_AdvancedPromotions_Helper_Data::RULE_MATCH_RETURNFORMAT_DEBUG, $this->getValue(), $multiplier, $groupIdentifier), Zend_Log::ERR, $returnFormat,'GROUP INDEPENDENT ACTION');
            }
            $tmpMatches[]=$action->validateItems($quote, $address, $returnFormat, $this->getValue(), $multiplier, $groupIdentifier);
            $nrConditions ++;
        }
        if ($nrConditions == 0) {
            //no conditions given default to matching all items
            foreach ($quote->getAllVisibleItems() as $quoteItem) {
                $ruleMatchesXtimes = Mage::helper('fooman_advancedpromotions')->formattedQuoteItem($matches, $ruleMatchesXtimes, $quoteItem, $this->getValue(), $groupIdentifier, $multiplier, $attrValue, $debug);
            }
        } else {
            $groupsCombined = array();
            $nrGroupMatched = 0;
            //combine the different matches into 1 group
            foreach ($tmpMatches as $groupMatched) {
                if (isset($groupMatched['matches']) && $groupMatched['rule_qty']) {
                    $groupsCombined = array_merge_recursive($groupsCombined, $groupMatched['matches']);
                }
            }
            //$matches = Mage::helper('fooman_advancedpromotions')->mergeTmpMatches($tmpMatches);


            foreach ($groupsCombined as $attrKey => $groupMatched) {
                foreach ($groupMatched as $attrValue => $items) {
                    if (sizeof($items) < $nrConditions) {
                        continue;
                    }
                    $tmpRuleMatchesXtimes = 0;
                    $tmpGroupMatches = array ();
                    $insufficientMatch = false;
                    foreach ($items as $key => $item) {
                        if(floor($item['rule_qty']/$multiplier) < 1 ) {
                            $insufficientMatch = true;
                        }
                        $tmpRuleMatchesXtimes += $item['rule_qty'];
                        $tmpGroupMatches[$attrKey][$attrValue][$key] = $item;
                    }
                    if (!$insufficientMatch) {
                        $tmpRuleMatchesXtimes += $tmpRuleMatchesXtimes;
                        $matches = array_merge_recursive($matches,$tmpGroupMatches);
                    }

                }
            }
        }
        return Mage::helper('fooman_advancedpromotions')->formattedReturnValue($returnFormat, $ruleType, $ruleMatchesXtimes, $matches);
    }


}