<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Rule_Action_GroupMatched
    extends Fooman_AdvancedPromotions_Model_SalesRule_Rule_Action_GroupSimple
{

    const TYPE = 'fooman_advancedpromotions/salesRule_rule_action_groupMatched';
   
    public function asHtml()
    {
        $html = $this->getTypeElement()->getHtml().
           Mage::helper('fooman_advancedpromotions')->__("Discount is applied within group as matched under conditions.");
           if ($this->getId()!='1') {
               $html.= $this->getRemoveLinkHtml();
           }
        return $html;
    }
}