<?php

class Fooman_AdvancedPromotions_Model_SalesRule_Mysql4_Rule_Collection extends Mage_SalesRule_Model_Mysql4_Rule_Collection
{
    /**
     * override parent to address issue when using group by with having clause in conjunction with order by
     *
     * @param  $websiteId
     * @param  $customerGroupId
     * @param  $couponCode
     * @param  $now
     * @return Fooman_AdvancedPromotions_Model_SalesRule_Mysql4_Rule_Collection
     */
    public function setValidationFilter($websiteId, $customerGroupId, $couponCode='', $now=null)
    {
        parent::setValidationFilter($websiteId, $customerGroupId, $couponCode, $now);
        //remove order by
        $this->getSelect()->setPart(Zend_Db_Select::ORDER,array());
        //keep rest of the original query
        $subQuery = $this->getSelect()->assemble();
        //and create a subquery from it which is then ordered by
        $this->getSelect()->reset();
        //Mage::getResourceSingleton('core/resource')->getConnection('core_write')->query('SET GLOBAL group_concat_max_len=999999'); 
        $this->getSelect()->from(array('mysubquery'=>new Zend_Db_Expr('('.$subQuery.')')));
        $this->getSelect()->order('sort_order');
        return $this;
    }
}
