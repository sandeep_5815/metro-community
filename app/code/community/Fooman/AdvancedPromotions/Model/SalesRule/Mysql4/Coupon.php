<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Model_SalesRule_Mysql4_Coupon extends Mage_SalesRule_Model_Mysql4_Coupon
{
    protected function _construct()
    {
        $this->_init('salesrule/coupon', 'coupon_id');
    }    
}