<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Block_Adminhtml_Promo_Quote_Grid extends Mage_Adminhtml_Block_Promo_Quote_Grid
{

    protected function _prepareColumns ()
    {

        $this->addColumn('action',
                array(
                    'header' => Mage::helper('sales')->__('Action'),
                    'width' => '120px',
                    'type' => 'action',
                    'getter' => 'getId',
                    'actions' => array(
                        array(
                            'caption' => Mage::helper('fooman_advancedpromotions')->__('Activate'),
                            'url' => array('base' => '*/promo_quote/activate'),
                            'field' => 'rule_id'
                        ),
                        array(
                            'caption' => Mage::helper('fooman_advancedpromotions')->__('Deactivate'),
                            'url' => array('base' => '*/promo_quote/deactivate'),
                            'field' => 'rule_id'
                        ),
                        array(
                            'caption' => Mage::helper('fooman_advancedpromotions')->__('Duplicate'),
                            'url' => array('base' => '*/promo_quote/duplicate'),
                            'field' => 'rule_id'
                        ),
                        array(
                            'caption' => Mage::helper('fooman_advancedpromotions')->__('Delete'),
                            'url' => array('base' => '*/promo_quote/massdelete'),
                            'field' => 'rule_id'
                        )
                    ),
                    'filter' => false,
                    'sortable' => false,
                    'index' => 'stores',
                    'is_system' => true,
        ));
        return parent::_prepareColumns();
    }

}