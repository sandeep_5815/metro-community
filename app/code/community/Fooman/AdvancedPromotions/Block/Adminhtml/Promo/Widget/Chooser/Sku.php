<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

class Fooman_AdvancedPromotions_Block_Adminhtml_Promo_Widget_Chooser_Sku extends Mage_Adminhtml_Block_Promo_Widget_Chooser_Sku
{

    public function _construct ()
    {

        if ($this->getRequest()->getParam('simpleonly') == 'true') {
            $this->setDefaultFilter(array('type' => Mage_Catalog_Model_Product_Type::TYPE_SIMPLE));
        }
        if ($this->getRequest()->getParam('giftcardonly') == 'true') {
            $this->setDefaultFilter(array('type' => 'ugiftcert'));
        }        
    }

    protected function _addColumnFilterToCollection ($column)
    {

        if ($this->getRequest()->getParam('simpleonly') == 'true') {
            $this->getCollection()->addAttributeToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);
        }
        if ($this->getRequest()->getParam('giftcardonly') == 'true') {
            $this->getCollection()->addAttributeToFilter('type_id', 'ugiftcert');
        }        
        parent::_addColumnFilterToCollection($column);
        return $this;
    }

}