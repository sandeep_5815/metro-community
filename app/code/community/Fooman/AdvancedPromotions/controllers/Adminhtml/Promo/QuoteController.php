<?php

/*
 * @author     Kristof Ringleff
 * @package    Fooman_AdvancedPromotions
 * @copyright  Copyright (c) 2010 Fooman Limited (http://www.fooman.co.nz)
 * @copyright  Copyright (c) 2010 smARTstudiosUK Limited (http://smartebusiness.co.uk)
 */

require_once BP.'/app/code/core/Mage/Adminhtml/controllers/Promo/QuoteController.php';

class Fooman_AdvancedPromotions_Adminhtml_Promo_QuoteController extends Mage_Adminhtml_Promo_QuoteController
{
    public function duplicateAction()
    {
        $oldRuleId = $this->getRequest()->getParam('rule_id');
        $oldRuleModel = Mage::getModel('salesrule/rule')->load($oldRuleId);

        if ($oldRuleModel->getId()) {
            try {
                $newRuleModel = Mage::getModel('salesrule/rule');
                $oldData = $oldRuleModel->getData();
                //delete the items that can't be used on the duplicate
                unset($oldData['rule_id']);
                unset($oldData['coupon_code']);
                $newRuleModel->setData($oldData);
                $newRuleModel->setConditions($oldRuleModel->getConditions());
                $newRuleModel->setActions($oldRuleModel->getActions());
                $newRuleModel->save();
                
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('catalogrule')->__('An error occurred while saving the rule data. Please review the log and try again.'));
                Mage::logException($e);
            }
        }
        $this->_getSession()->addSuccess(Mage::helper('fooman_advancedpromotions')->__('The rule has been duplicated.'));
        $this->_redirect('*/*/');
    }

    public function deactivateAction()
    {
        $ruleId = $this->getRequest()->getParam('rule_id');
        $rule = Mage::getModel('salesrule/rule')->load($ruleId);

        if ($rule->getId()) {
            try {
                $rule->setIsActive(false);
                $rule->save();
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('catalogrule')->__('An error occurred while saving the rule data. Please review the log and try again.'));
                Mage::logException($e);
            }
        }
        $this->_getSession()->addSuccess(Mage::helper('fooman_advancedpromotions')->__('The rule has been deactivated.'));
        $this->_redirect('*/*/');
    }

    public function activateAction()
    {
        $ruleId = $this->getRequest()->getParam('rule_id');
        $rule = Mage::getModel('salesrule/rule')->load($ruleId);

        if ($rule->getId()) {
            try {
                $rule->setIsActive(true);
                $rule->save();
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('catalogrule')->__('An error occurred while saving the rule data. Please review the log and try again.'));
                Mage::logException($e);
            }
        }
        $this->_getSession()->addSuccess(Mage::helper('fooman_advancedpromotions')->__('The rule has been activated.'));
        $this->_redirect('*/*/');
    }


    public function massdeleteAction()
    {
        $ruleId = $this->getRequest()->getParam('rule_id');
        $rule = Mage::getModel('salesrule/rule')->load($ruleId);

        if ($rule->getId()) {
            try {
                $rule->delete();
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('catalogrule')->__('An error occurred while saving the rule data. Please review the log and try again.'));
                Mage::logException($e);
            }
        }
        $this->_getSession()->addSuccess(Mage::helper('fooman_advancedpromotions')->__('The rule has been deleted.'));
        $this->_redirect('*/*/');
    }
}